import json

import requests

from helpers.api.common_helpers import api_url, headers, cookies


def get_all_products():
    url = api_url + "/api/admin/product/all"

    params = {
        "limit": 2000
    }

    return requests.get(url=url, params=params, headers=headers, cookies=cookies)


def change_stock_status(prod_id, stock_status):
    """ 1 - In stock
        2 - Out of stock
        3 - On Backorder
        4 - Made to Order"""

    url = api_url + "/api/admin/product/{}/update-setting".format(prod_id)

    data = {
        "stockStatus": stock_status
    }

    return requests.put(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def get_all_prod_ids():
    prod_ids = []

    resp_json = get_all_products().json()

    for elem in resp_json["rows"]:
        prod_ids.append(elem["id_product"])

    return prod_ids


def change_stock_status_bulk(prod_list, stock_status):
    for prod in prod_list:
        change_stock_status(prod, stock_status)


def get_wed_dress_categ_ids():
    ids = [6]

    url = api_url + "/api/categories/category-tree-admin"

    resp_json = requests.get(url=url, headers=headers, cookies=cookies).json()

    for categ in resp_json["tree"]:
        if categ["id_category"] == ids[0]:
            for sub_categ in categ["children"]:
                if sub_categ["id_category"] not in ids:
                    ids.append(sub_categ["id_category"])
                if sub_categ["children"] is not None:
                    for sub_sub_categ in sub_categ["children"]:
                        ids.append(sub_sub_categ["id_category"])

    return ids


def activate_partial_amount(prod_id):
    url = api_url + "/api/admin/product/{}/update-setting".format(prod_id)

    data = {
        "partial_payment": {
            "partial": True,
            "custom": 30
        }
    }

    return requests.put(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def deactivate_partial_amount(prod_id):
    url = api_url + "/api/admin/product/{}/update-setting".format(prod_id)

    data = {
        "partial_payment": {
            "partial": False,
            "custom": 30
        }
    }

    return requests.put(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def activate_partial_amount_bulk(prods):
    for prod in prods:
        activate_partial_amount(prod)


def deactivate_partial_amount_bulk(prods):
    for prod in prods:
        deactivate_partial_amount(prod)


def get_all_wedd_dress_prod_ids():
    ids = []
    wed_dress_cat_ids = get_wed_dress_categ_ids()
    resp_json = get_all_products().json()
    for prod in resp_json["rows"]:
        if prod["Category"] is None:
            print(prod["id_product"])
        elif prod["Category"]["id_category"] in wed_dress_cat_ids:
            ids.append(prod["id_product"])

    return ids


def test_wed_dress_ids():
    a = get_all_wedd_dress_prod_ids()
    print("There are", len(a))
    print(a)


def test_check_categ():
    a = get_wed_dress_categ_ids()
    print(len(a))
    print(a)


def test_change_stock_status_bulk():
    prod_list = [1454, 1455, 1457, 1462, 1602, 1603]
    # prod_list = get_all_prod_ids()

    change_stock_status_bulk(prod_list, 1)


def test_activate_partial_amount_bulk():
    prod_list = [1454, 1455, 1457, 1462, 1602, 1603]
    # prod_list = get_all_prod_ids()

    activate_partial_amount_bulk(prod_list)
    # deactivate_partial_amount_bulk(prod_list)




def test_check():
    qua = []

    resp_json = get_all_products().json()

    print("\n", resp_json["pageCount"])

    for elem in resp_json["rows"]:
        qua.append(elem["id_product"])

    print(len(qua))
    print(sorted(qua))
