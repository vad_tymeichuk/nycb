import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies
from helpers.api.common_helpers import headers, cookies
from helpers.api.product_helpers import get_parent_categories_tree_admin
from helpers.utils import settings

main_url = settings["url"]
api_url = settings["api-url"]
print("main_url=" + main_url)
print("api_url=" + api_url)


def test_verify_categories_for_500():
    obj = get_parent_categories_tree_admin().json()
    no_errors = True
    for x in range(len(obj["tree"])):
        url = main_url + obj["tree"][x]["slug"]
        r = requests.get(url=url)
        if r.status_code != 200:
            print("{} at URL={}".format(str(r.status_code), url))
            no_errors = False

    assert no_errors


def test_verify_products_for_500():
    url = api_url + "/api/admin/product/all"
    data = {}
    params = {
        "limit": 2000
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()

    print("Number of products found: {}".format(len(resp_json["rows"])))

    no_errors = True
    for x in range(len(resp_json["rows"])):
        url = main_url + resp_json["rows"][x]["slug"]
        r = requests.get(url=url)
        if r.status_code != 200:
            print("#{}. {} at URL={}".format(x, str(r.status_code), url))
            no_errors = False

    assert no_errors


#
def test_verify_products_for_null_variation_values():
    empty_variations_detection = False
    anomaly_detection = True

    url = api_url + "/api/admin/product/all"
    params = {
        "limit": 2000
    }
    r = requests.get(url=url, params=params, headers=headers, cookies=cookies)
    assert r.status_code == 200
    products_json = r.json()

    print("Number of products found: {}".format(len(products_json["rows"])))

    no_errors = True
    for x in range(len(products_json["rows"])):
        current_product = products_json["rows"][x]
        if current_product["img"] is None:
            # Skipping products without images
            continue
        url = api_url + "/api/admin/product/{}/variation".format(current_product["id_product"])
        r = requests.get(url=url, headers=headers, cookies=cookies)
        assert r.status_code == 200
        variations_json = r.json()
        nom_of_variations = len(variations_json["getVariation"])
        if empty_variations_detection:
            if nom_of_variations > 0:
                for i in variations_json["getVariation"]:
                    if len(i["variation_product_throughs"]) == 0:
                        print("Empty variations for product: " + main_url + "/" + current_product["slug"])
                        no_errors = False
                        break
            else:
                print("No variations for product: " + main_url + "/" + current_product["slug"])
                no_errors = False
                continue
        if anomaly_detection:
            if variations_json["getVariation"][0]["variation_product_throughs"] != 0 and detect_anomaly(variations_json) is not None:
                print("Product with anomaly: " + main_url + "/" + current_product["slug"])
                no_errors = False
        # print(resp_json)
    assert no_errors

# pytest tests_in_development/test_500_product_generator.py::test_verify_products_for_null_variation_values


def detect_anomaly(variations_json):
    unique_variations = []
    for i in variations_json["getVariation"]:
        unique_var_key = ""
        for j in i["variation_product_throughs"]:
            unique_var_key = unique_var_key + str(j["attribute_term"])
        if unique_var_key not in unique_variations:
            unique_variations.append(unique_var_key)
        else:
            print("Variation {} was repeated twice".format(unique_var_key))
            return unique_var_key
    return None
