import json

import pytest
import requests
from helpers.api.common_helpers import api_url, headers


def test_verify_homepage_metadata():
    url = api_url + "/api/main"
    data = {}
    r = requests.get(url=url, data=json.dumps(data), headers=headers)
    assert r.status_code == 200
    resp_json = r.json()
    assert "mainSlider" in resp_json
    assert "mainSliderRecomedUs" in resp_json
    assert "mainCommentSlider" in resp_json
    assert "blogSectionMain" in resp_json
    assert "BrandSliderMain" in resp_json


@pytest.mark.skip(reason="waiting for staging server update")
def test_verify_our_brides_images_list():
    url = api_url + "/api/our-brides/get-list"
    r = requests.get(url=url, headers=headers)
    assert r.status_code == 200
    resp_json = r.json()
    assert "our_brides_images" in resp_json
    assert "rows" in resp_json["our_brides_images"]
    assert "nextPage" in resp_json["our_brides_images"]
    # assert "id" in resp_json["our_brides_images"]["rows"][0]
    # assert "img_path" in resp_json["our_brides_images"]["rows"][0]
    # assert "title" in resp_json["our_brides_images"]["rows"][0]
    # assert "createdAt" in resp_json["our_brides_images"]["rows"][0]
