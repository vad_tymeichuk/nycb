from helpers.api.product_helpers import *
from helpers.api.upload_image_helpers import upload_image, delete_image, get_image_id
from helpers.api.common_helpers import date_time

review_text = "This wedding dress is wonderful!"
review_text_upd = review_text + date_time
title_updated = "updated"
product_id = 962
upload_path = "/api/reviews/uploads"


def get_review_count():
    url = api_url + "/api/reviews/?limit=1&page=1"
    r = requests.get(url=url, headers=headers, cookies=cookies)
    resp_json = r.json()

    return resp_json["count"]


def get_reviews_for_products():
    limit = random.choice(range(0, get_review_count()))
    pages = get_review_count() // limit
    url = api_url + "/api/reviews/?limit={}&page={}".format(limit, random.choice(range(0, pages)))

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_reviews_for_product_by_slug():
    product_slug = get_product_slug_by_id(product_id)
    url = api_url + "/api/item/" + product_slug + "/get-reviews"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_image_info_by_id(image_id):
    url = api_url + "/api/our-brides/get-photo/" + str(image_id)

    return requests.post(url=url, headers=headers, cookies=cookies)


def approve_image(image_id):
    url = api_url + "/api/our-brides/update/" + str(image_id)
    body = {
        "product_show": True,
    }

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def update_image_by_id(image_id):
    url = api_url + "/api/our-brides/update/" + str(image_id)
    body = {
        "title": title_updated,
    }

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def get_review_list():
    url = api_url + "/api/reviews/get-reviews"

    return requests.post(url=url, headers=headers, cookies=cookies)


def get_review_id():
    # create_review()

    r = get_review_list()
    resp_json = r.json()
    for elem in resp_json["reviews"]:
        if elem["text"] == review_text:
            return elem["id"]


def approve_review(review_id):
    # image_id = get_image_id(upload_image().json())
    url = api_url + "/api/reviews/update-review/" + str(review_id)
    body = {
        "moderation": True,
    }

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def create_review():
    # r = upload_image()
    # resp_json = r.json()
    photo_id = get_image_id(upload_image(upload_path).json())
    approve_image(photo_id)
    url = api_url + "/api/reviews/create-review"
    body = {
        "text": review_text,
        "sign_up_newsletter": True,
        "rating": 4,
        "productsId": product_id,
        "userId": 1,
        "images": [photo_id]
    }

    r = requests.post(url=url, data=json.dumps(body), headers=headers, cookies=cookies)
    approve_review(get_review_id())

    return [r, photo_id]


def update_review(review_id):
    url = api_url + "/api/reviews/update-review/" + str(review_id)
    body = {
        "text": review_text_upd,
        "rating": 5
    }

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def delete_review(review_id):
    url = api_url + "/api/reviews/update-review/" + str(review_id)
    body = {"trash": True}

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def get_product_slug_by_id(id_):
    url = api_url + "/api/admin/product/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies).json()["product"]["slug"]


def test_get_reviews_for_products():
    elems_item = ["id_product", "product_name", "slug", "img", "product_desc"]
    elems_review_last = ["id", "createdAt", "rating", "text", "user"]

    r = get_reviews_for_products()
    assert r.status_code == 200
    resp_json = r.json()
    for elem in elems_item:
        assert elem in resp_json["products"][0]["item"]
    for elem_ in elems_review_last:
        assert elem_ in resp_json["products"][0]["review_last"]


def test_upload_image():
    r = upload_image("/api/reviews/uploads")
    assert r.status_code == 200
    resp_json = r.json()
    assert "id" in resp_json["images"][0]
    assert "img_path" in resp_json["images"][0]

    delete_image(get_image_id(resp_json))


def test_create_delete_review():
    review = create_review()
    r = review[0]
    assert r.status_code == 201
    resp_json = r.json()
    assert resp_json["created"]

    review_id = get_review_id()
    print("Review id:", review_id)
    approve_review(review_id)
    delete_image(review[1])
    r = delete_review(review_id)
    resp_json = r.json()
    assert r.status_code == 200
    assert resp_json["update"]


def test_update_review():
    create_review()

    review_id = get_review_id()
    r = update_review(review_id)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["update"]

    resp_json = get_reviews_for_product_by_slug().json()
    for elem in resp_json["reviews"]:
        if review_id == elem["id"]:
            assert review_text_upd == elem["text"]
            assert elem["rating"] == 5

    delete_review(review_id)


def test_get_image_info():
    image_id = get_image_id(upload_image(upload_path).json())

    elems = ["id", "title", "type", "our_bride_show", "product_show", "img_path", "product_link", "trash", "createdAt", "deletedAt", "products", "rReviewId"]

    r = get_image_info_by_id(image_id)
    assert r.status_code == 200
    resp_json = r.json()
    try:
        for elem in elems:
            assert elem in resp_json["image"]
        assert "review" == resp_json["image"]["type"]
    finally:
        print("deleted")
        delete_image(image_id)


def test_update_image_by_id():
    image_id = get_image_id(upload_image(upload_path).json())

    r = update_image_by_id(image_id)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["updated"]

    resp_json = get_image_info_by_id(image_id).json()
    assert title_updated == resp_json["image"]["title"]

    delete_image(image_id)


def test_get_review_by_slug():
    slug = get_product_slug_by_id(product_id)
    print(slug)

    url = api_url + "/api/item/{}/get-reviews".format(get_product_slug_by_id(product_id))

    r = requests.get(url=url, headers=headers, cookies=cookies)
    assert 200 == r.status_code
    resp_json = r.json()

    if len(resp_json["reviews"]) == 0 or resp_json["count"] == 0:
        create_review()
        r = requests.get(url=url, headers=headers, cookies=cookies)
        assert 200 == r.status_code
        resp_json = r.json()

    assert review_text == resp_json["reviews"][0]["text"]
