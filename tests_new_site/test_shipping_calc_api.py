import json
import random

import pytest
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time
from helpers.api.cart_helpers import add_to_cart, delete_from_cart, get_cart, clear_cart, get_ship_cost_for_case_id

class_name = "2021 Wedding Dresses Shipping " + date_time
process_time_label = "up to 2 business day"
shipping_label_est = "10 business days"
countryId = random.randrange(1, 220)
class_attr = ["id", "name", "hcProcessingTimeId", "hc_processing_time", "country"]
proc_time_elems = ["id", "name"]
# shipping class id for creating and updating (deleting) methods and cases (has to be present in DB)
shipping_class_id = 25
method_1 = "Test Express"
method_1_new = "Test Express NEW"
method_2 = "Test Economy"
method_2_new = "Test Economy NEW"
type_method_1 = "free shipping"
type_method_2 = "fixed price"
shipCaseActiveId = 725


def create_shipping_class():
    global class_name
    url = api_url + "/api/shipping-calculator/create-class"
    body = {
        "name": class_name,
        "processing_time_label": process_time_label,
        "shipping_label_estimate": shipping_label_est,
        "countryId": countryId,
        "hcProcessingTimeId": 1
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def delete_shipping_class():
    class_id = get_ship_class_id()
    url = api_url + "/api/shipping-calculator/" + str(class_id)
    return requests.delete(url=url, headers=headers, cookies=cookies)


def get_all_ship_classes():
    url = api_url + "/api/shipping-calculator/get-classes"
    return requests.post(url=url, headers=headers, cookies=cookies)


def get_ship_class_id():
    class_id = None
    r = get_all_ship_classes()
    class_list = r.json()
    for ship_class in class_list["shipping_classes"]:
        if class_name == ship_class["name"]:
            class_id = ship_class["id"]
            return class_id
    return class_id


def get_shipping_class_by_id():
    id_ = get_ship_class_id()
    url = api_url + "/api/shipping-calculator/get-class/" + str(id_)
    return requests.post(url=url, headers=headers, cookies=cookies)


def update_ship_class():
    global class_name
    global process_time_label
    global shipping_label_est
    global countryId
    class_name = class_name + " updated"
    process_time_label = process_time_label + " + 1"
    shipping_label_est = shipping_label_est + " + 1"
    countryId = countryId + 1
    create_shipping_class()
    class_id = get_ship_class_id()
    # after implementing deleting class replace str(class_id) to str(get_shipping_class_id())
    url = api_url + "/api/shipping-calculator/update-class/" + str(class_id)
    params = {
        "name": class_name,
        "processing_time_label": process_time_label,
        "shipping_label_estimate": shipping_label_est,
        "countryId": countryId,
        "hcProcessingTimeId": 1
    }
    return requests.put(url=url, headers=headers, data=json.dumps(params), cookies=cookies)


def create_shipping_methods():
    url = api_url + "/api/shipping-calculator/create-methods-cases"
    body = {
        "methods": [{
            "name": method_1,
            "cases": [{
                "countries": [1, 2, 3],
                "from_delivery_time": 2,
                "to_delivery_time": 3,
                "type_method": type_method_1,
                "one_item": 23.4,
                "additional_item": 56.3
            }]
        }, {
            "name": method_2,
            "cases": [{
                "countries": [20],
                "from_delivery_time": 2,
                "to_delivery_time": 3,
                "type_method": type_method_2,
                "one_item": 23.4,
                "additional_item": 56.3
            }]
        }],
        "classId": shipping_class_id
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def get_ship_methods_by_ship_class_id():
    url = api_url + "/api/shipping-calculator/get-methods"
    body = {
        "hcShippingClassId": shipping_class_id
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def update_shipping_method():
    delete_shipping_methods()

    create_shipping_methods()

    r = get_ship_methods_by_ship_class_id()

    resp_json = r.json()
    ship_method_id_1 = resp_json["ship_methods"][0]["id"]
    method_case_id_1 = resp_json["ship_methods"][0]["cases"][0]["id"]
    ship_method_id_2 = resp_json["ship_methods"][1]["id"]
    method_case_id_2 = resp_json["ship_methods"][1]["cases"][0]["id"]

    url = api_url + "/api/shipping-calculator/update-methods-cases"

    body = {
        "shipping_methods": [
            {
                "id": ship_method_id_1,
                "name": method_1_new,
                "slug": "test-express-new",
                "cases": [
                    {
                        "id": method_case_id_1,
                        "from_delivery_time": 23,
                        "to_delivery_time": 324,
                        "type_method": type_method_1,
                        "one_item": 23,
                        "additional_item": 55,
                        "countries": [
                            {
                                "value": 5
                            },
                            {
                                "value": 6
                            }
                        ]
                    }
                ]
            }, {
                "id": ship_method_id_2,
                "name": method_2_new,
                "slug": "test-economy-new",
                "cases": [
                    {
                        "id": method_case_id_2,
                        "from_delivery_time": 23,
                        "to_delivery_time": 324,
                        "type_method": type_method_2,
                        "one_item": 23,
                        "additional_item": 55,
                        "countries": [
                            {
                                "value": 6
                            }
                        ]
                    }
                ]
            }
        ],
        "classId": shipping_class_id
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


# deleting means passing no elements to the "methods" list in the body
def delete_shipping_methods():
    url = api_url + "/api/shipping-calculator/update-methods-cases"
    body = {
        "shipping_methods": [],
        "classId": shipping_class_id
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def calculate_shipping_for_cart():
    url = api_url + "/api/cart/calculate"
    body = {
        "geo": {
            "country_code": "US",
            "country_Id": 226,
            "city": "Berkeley",
            "state_code": "",
            "postcode": "",
            "shipCaseActiveId": shipCaseActiveId
        }
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def test_get_all_ship_classes():
    classes = get_all_ship_classes()
    for elem in classes.json()["shipping_classes"]:
        for attr in class_attr:
            assert attr in elem


def test_get_shipping_class_by_id():
    create_shipping_class()
    r = get_shipping_class_by_id()
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["shippingClass"]["id"] == get_ship_class_id()
    assert resp_json["shippingClass"]["name"] == class_name
    assert resp_json["shippingClass"]["shipping_label_estimate"] == shipping_label_est
    assert resp_json["shippingClass"]["processing_time_label"] == process_time_label
    assert resp_json["shippingClass"]["countryId"] == countryId
    assert resp_json["shippingClass"]["hcProcessingTimeId"] == 1

    delete_shipping_class()


def test_create_ship_class():
    r = create_shipping_class()
    assert r.status_code == 201
    resp_json = r.json()
    assert resp_json["created"]

    delete_shipping_class()


"""return type is not json"""


def test_delete_ship_class():
    create_shipping_class()
    r = delete_shipping_class()
    assert r.status_code == 200
    # resp_json = r.json()
    assert "OK" == r.text


def test_update_shipping_class():
    r = update_ship_class()
    resp_json = r.json()

    assert r.status_code == 201
    assert resp_json["updated"] is True

    class_info = get_shipping_class_by_id()

    resp_json = class_info.json()
    assert class_name == resp_json["shippingClass"]["name"]
    assert shipping_label_est == resp_json["shippingClass"]["shipping_label_estimate"]
    assert process_time_label == resp_json["shippingClass"]["processing_time_label"]
    assert countryId == resp_json["shippingClass"]["countryId"]

    delete_shipping_class()


def test_get_processing_time():
    url = api_url + "/api/shipping-calculator/get-processing-times"
    r = requests.post(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in resp_json["processing_times"]:
        for elem1 in proc_time_elems:
            assert elem1 in elem


def test_get_country_codes():
    country_elems = ["id", "name", "alpha_2_code"]
    url = api_url + "/api/country/alfa"
    r = requests.get(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in resp_json:
        for elem1 in country_elems:
            assert elem1 in elem


def test_create_shipping_methods():
    r = create_shipping_methods()
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["created"]

    delete_shipping_methods()


"""return type is not json"""


def test_delete_shipping_methods():
    create_shipping_methods()
    r = delete_shipping_methods()
    assert r.status_code == 200
    # resp_json = r.json()
    assert "OK" == r.text


def test_get_shipping_methods():
    case_elems = ["id", "from_delivery_time", "to_delivery_time", "type_method", "one_item", "additional_item",
                  "hcShippingMethodId", "countries"]

    create_shipping_methods()

    r = get_ship_methods_by_ship_class_id()

    assert r.status_code == 200
    resp_json = r.json()
    assert method_1 == resp_json["ship_methods"][0]["name"]
    assert shipping_class_id == resp_json["ship_methods"][0]["hcShippingClassId"]
    assert type_method_1 == resp_json["ship_methods"][0]["cases"][0]["type_method"]
    assert method_2 == resp_json["ship_methods"][1]["name"]
    assert shipping_class_id == resp_json["ship_methods"][1]["hcShippingClassId"]
    assert type_method_2 == resp_json["ship_methods"][1]["cases"][0]["type_method"]
    for elem in resp_json["ship_methods"]:
        for elem1 in elem["cases"]:
            for elem2 in case_elems:
                assert elem2 in elem1

    delete_shipping_methods()


def test_update_shipping_methods():
    case_elems = ["id", "from_delivery_time", "to_delivery_time", "type_method", "one_item", "additional_item",
                  "hcShippingMethodId", "countries"]

    r = update_shipping_method()
    assert r.status_code == 200
    assert "OK" == r.text

    r = get_ship_methods_by_ship_class_id()

    resp_json = r.json()
    assert method_1_new == resp_json["ship_methods"][0]["name"]
    assert shipping_class_id == resp_json["ship_methods"][0]["hcShippingClassId"]
    assert type_method_1 == resp_json["ship_methods"][0]["cases"][0]["type_method"]
    assert method_2_new == resp_json["ship_methods"][1]["name"]
    assert shipping_class_id == resp_json["ship_methods"][1]["hcShippingClassId"]
    assert type_method_2 == resp_json["ship_methods"][1]["cases"][0]["type_method"]
    for elem in resp_json["ship_methods"]:
        for elem1 in elem["cases"]:
            for elem2 in case_elems:
                assert elem2 in elem1

    delete_shipping_methods()


def test_get_ship_class_list():
    elems = ["id", "name"]
    url = api_url + "/api/shipping-calculator/get-class-list"
    r = requests.post(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in resp_json["shipping_class_list"]:
        for elem1 in elems:
            assert elem1 in elem


def test_get_country_list():
    elems = ["id", "name"]
    url = api_url + "/api/country"
    r = requests.post(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in resp_json:
        for elem1 in elems:
            assert elem1 in elem


# @pytest.mark.skip(reason="fails due to bug = '0' returned in 'total line")
def test_calculate_shipping_for_cart():
    clear_cart()
    elems = ["subTotal", "total", "shippingCalculator", "tax"]
    add_to_cart()
    cart = get_cart().json()
    cart_sub_total = cart["subTotal"]
    ship_cost = get_ship_cost_for_case_id(shipCaseActiveId)
    print(cart_sub_total)
    print(ship_cost)

    r = calculate_shipping_for_cart()
    assert r.status_code == 200
    resp_json = r.json()
    subTotal = resp_json["object"]["subTotal"]
    total = resp_json["object"]["total"]

    assert str(cart_sub_total) == subTotal
    assert float(cart_sub_total) + ship_cost == float(total)
    for elem in elems:
        assert elem in resp_json["object"]
        if elem == elems[2]:
            for elem1 in resp_json["object"][elems[2]]:
                if shipCaseActiveId == elem1["caseId"]:
                    assert elem1["active"]

    delete_from_cart()

