import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time

color = "red"
color_upd = "green"
text = "api test"
text_upd = text + " " + date_time
text_card = "api test"
text_card_upd = text_card + " " + date_time
cat_id = 49
stock_status = "Made to Order"
stock_status_upd = "In stock"


def create_label():
    url = api_url + "/api/product-label/create-label"
    data = {
        "main_label": {
            "color": color,
            "text": text
        },
        "text_card": text_card,
        "settings": [
            [
                {
                    "productLabelConditionId": 1,
                    "productLabelConditionOptionId": None,
                    "stock_status": stock_status,
                    "products": [962],
                    "categories": [{
                        "id": cat_id,
                        "include_subcategories": True
                    }]
                },
                {
                    "productLabelConditionId": 5,
                    "productLabelConditionOptionId": None,
                    "stock_status": stock_status,
                    "products": [962],
                    "categories": [{
                        "id": cat_id,
                        "include_subcategories": True
                    }]
                }
            ]
        ]
    }

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def update_label(id_):
    url = api_url + "/api/product-label/update-label/" + str(id_)
    data = {
        "main_label": {
            "color": color_upd,
            "text": text_upd
        },
        "text_card": text_card_upd,
        "settings": [
            [
                {
                    "productLabelConditionId": 1,
                    "productLabelConditionOptionId": None,
                    "stock_status": stock_status_upd,
                    "products": [962],
                    "categories": [{
                        "id": cat_id,
                        "include_subcategories": True
                    }]
                },
                {
                    "productLabelConditionId": 5,
                    "productLabelConditionOptionId": None,
                    "stock_status": stock_status_upd,
                    "products": [962],
                    "categories": [{
                        "id": cat_id,
                        "include_subcategories": True
                    }]
                }
            ]
        ]
    }

    return requests.put(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def get_conditions_options():
    url = api_url + "/api/product-label/get-conditions-options"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_label_info(id_):
    url = api_url + "/api/product-label/get/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_all_labels():
    url = api_url + "/api/product-label/get-product-labels-to-admin"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_label_id():
    resp_json = get_all_labels().json()

    for elem in resp_json["labels"]:
        if elem["main_label"]["text"] == text and elem["text_card"] == text_card:
            return elem["id"]

    return None


def delete_label(id_):
    url = api_url + "/api/product-label/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def test_create_delete_label():
    """returns text not json"""
    r = create_label()

    assert 201 == r.status_code
    assert "Created" == r.text

    label_id = get_label_id()

    r = delete_label(label_id)

    assert 200 == r.status_code
    assert "OK" == r.text


def test_get_label_info():
    create_label()

    label_id = get_label_id()

    r = get_label_info(label_id)

    assert 200 == r.status_code
    resp_json = r.json()
    assert text == resp_json["label"]["main_label"]["text"]
    assert color == resp_json["label"]["main_label"]["color"]
    assert text_card == resp_json["label"]["text_card"]
    assert resp_json["label"]["active"]
    assert cat_id == resp_json["label"]["settings"][0][0]["Categories"][0]["id_category"]
    assert stock_status == resp_json["label"]["settings"][0][1]["stock_status"]

    delete_label(label_id)


def test_update_label():
    create_label()

    label_id = get_label_id()

    r = update_label(label_id)

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["update"]

    r = get_label_info(label_id)

    assert 200 == r.status_code
    resp_json = r.json()
    assert text_upd == resp_json["label"]["main_label"]["text"]
    assert color_upd == resp_json["label"]["main_label"]["color"]
    assert text_card_upd == resp_json["label"]["text_card"]
    assert resp_json["label"]["active"]
    assert cat_id == resp_json["label"]["settings"][0][0]["Categories"][0]["id_category"]
    assert stock_status_upd == resp_json["label"]["settings"][0][1]["stock_status"]

    delete_label(label_id)


def test_get_conditions_options():
    r = get_conditions_options()

    elems = ["id", "name", "deletedAt", "product_label_condition_options"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["condition"]:
            assert elem in elem1
