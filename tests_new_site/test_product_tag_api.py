import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time

tag_name = "test tag " + date_time
tag_descr = "test tag description " + date_time


def get_tag_list():
    url = api_url + "/api/product-tags/get-list"

    return requests.post(url=url, headers=headers, cookies=cookies)


def create_tag():
    url = api_url + "/api/product-tags/create"
    body = {
        "name_tag": tag_name,
        "description_tag": tag_descr,
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def get_tag_id():
    r = get_tag_list()
    resp_json = r.json()
    for elem in resp_json["product_tags"]:
        if tag_name == elem["name_tag"]:
            return elem["id"]
        else:
            return False


def delete_tag(id_):
    url = api_url + "/api/product-tags/delete/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def edit_tag(id_):
    # create_tag()

    url = api_url + "/api/product-tags/update/" + str(id_)
    body = {
        "name_tag": tag_name + " edited",
        "description_tag": tag_descr + " edited",
    }

    return requests.put(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def delete_tag_via_edit(id_, trash_flag):

    url = api_url + "/api/product-tags/update/" + str(id_)
    body = {
        "trash": trash_flag
    }

    return requests.put(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def test_get_tag_list():
    elems = ["id", "name_tag", "slug"]
    r = get_tag_list()

    assert r.status_code == 200
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["product_tags"]:
            assert elem in elem1


def test_create_tag():
    r = create_tag()

    assert r.status_code == 201
    resp_json = r.json()
    assert resp_json["created"]

    tag_id = get_tag_id()

    delete_tag(tag_id)


def test_edit_tag():
    create_tag()

    tag_id = get_tag_id()

    r = edit_tag(tag_id)

    assert r.status_code == 200
    resp_json = r.json()
    assert "updated!" == resp_json["msg"]
    assert resp_json["updated"]

    delete_tag(tag_id)


def test_delete_tag():
    create_tag()

    tag_id = get_tag_id()

    r = delete_tag(tag_id)
    assert r.status_code == 200
    resp_json = r.json()
    assert "deleted!" == resp_json["msg"]
    assert resp_json["delete"]
    assert get_tag_id() is False


def test_delete_via_edit():
    create_tag()

    tag_id = get_tag_id()

    r = delete_tag_via_edit(tag_id, 1)

    assert r.status_code == 200
    resp_json = r.json()
    assert "updated!" == resp_json["msg"]
    assert resp_json["updated"]
    assert get_tag_id() is False


def test_no_update_msg():
    create_tag()

    tag_id = get_tag_id()

    r = delete_tag_via_edit(tag_id, 0)

    assert r.status_code == 404
    resp_json = r.json()
    assert "no updated!" == resp_json["msg"]

    delete_tag(tag_id)

