import math
import random

import requests
from helpers.api.common_helpers import api_url, headers, cookies

category_slug = random.choice(["veils", "evening-dresses", "wedding-dresses", "supernova-plus-size", "hair-accessories"])


def test_get_category_by_slug():
    print("slug =", category_slug)
    url = api_url + "/api/catalog/get/" + category_slug
    limit = 24
    params = {"limit": str(limit), "page": 1}
    r = requests.get(url=url, params=params, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    assert not resp_json["redirectObject"]["redirect"]
    assert resp_json["redirectObject"]["slugCategory"] == category_slug

    # """uncomment when bug is fixed - https://riseserv.atlassian.net/browse/NYCB-438 """

    assert resp_json["pageCount"] == math.ceil(resp_json["count"]/limit)
    if resp_json["count"] >= limit:
        assert len(resp_json["rows"]) == limit
    else:
        assert len(resp_json["rows"]) == resp_json["count"]


def test_get_filters_by_slug():
    print("slug =", category_slug)
    url = api_url + "/api/catalog/get-filters/" + category_slug
    attr_elems = ["id", "name", "slug", "order_by", "attribute_terms"]
    tag_elems = ["name", "slug", "attribute_terms"]
    r = requests.get(url=url, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    assert not resp_json["redirectObject"]["redirect"]
    assert category_slug == resp_json["redirectObject"]["slugCategory"]
    assert "rangePrice" in resp_json["redirectObject"]
    for el in attr_elems:
        for el1 in resp_json["attributes"]:
            if len(el1) == 5:
                assert el in el1
    for el2 in tag_elems:
        for el1 in resp_json["attributes"]:
            if len(el1) == 3:
                assert el2 in el1


def test_get_active_category_tree():
    print("slug =", category_slug)
    url = api_url + "/api/catalog/category/" + category_slug
    category_elems = ["id_category", "title_category", "parentId", "slug", "children"]
    r = requests.get(url=url, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    for el in category_elems:
        assert el in resp_json["tree"][0]
        if resp_json["tree"][0]["title_category"] == "title_category":
            assert category_slug == resp_json["tree"][0]["slug"]


def test_get_products_by_attrs():
    print("slug =", category_slug)
    url = api_url + "/api/catalog/get/" + category_slug + "/color=white,black;size=2,0"
    product_attr = ["id_product", "slug", "product_name", "img", "stock_status", "categoryId", "cost"]
    limit = 34
    params = {"page": 1, "limit": str(limit)}
    r = requests.get(url=url, params=params, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    for el1 in resp_json["rows"]:
        for attr in product_attr:
            assert attr in el1


def test_get_category_seo_info():
    print("slug =", category_slug)
    url = api_url + "/api/catalog/get-inf/" + category_slug
    seo_attr = ["id_category", "title_category", "category_desc", "client_text", "seo_title", "seo_desc", "seo_key", "img", "slug"]
    r = requests.get(url=url, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    assert "sale" in resp_json
    assert "breadcrumbs" in resp_json
    for el in seo_attr:
        assert el in resp_json["category"]


def test_verify_catalog_search():
    url = api_url + "/api/catalog/search"
    key_word_1 = "wedding"
    key_word_2 = "dress"
    limit = 4
    search_keyword = key_word_1 + " " + key_word_2
    product_attr = ["id_product", "categoryId", "_v", "slug", "cost", "type", "product_name", "name_attr", "img", "productLabels"]
    params = {"s": search_keyword, "page": 1, "limit": str(limit)}
    r = requests.get(url=url, params=params, headers=headers)
    resp_json = r.json()
    assert r.status_code == 200
    assert resp_json["pageCount"] == math.ceil(resp_json["count"]/limit)
    """potential bug"""
    if limit <= resp_json["count"]:
        assert len(resp_json["rows"]) == limit
    for el1 in resp_json["rows"]:
        assert key_word_1 or key_word_2 in el1["product_name"].lower()
        for attr in product_attr:
            assert attr in el1
