import requests
from helpers.api.common_helpers import api_url, headers, cookies


def test_main_menu():
    url = api_url + "/api/main/main-menu"

    r = requests.get(url=url, headers=headers, cookies=cookies)

    assert 200 == r.status_code
    resp_json = r.json()
    assert "All wedding dresses" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][0]["name_en"]
    assert "New Arrivals" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][1]["name_en"]
    assert "Ready to Ship" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][2]["name_en"]
    assert "Dresses under $1000" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][3]["name_en"]
    assert "Sale" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][4]["name_en"]
    assert "Size Guide" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][5]["name_en"]
    assert "Plus Size Wedding Dresses" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][6]["name_en"]
    assert "Alteration Shops" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][7]["name_en"]
    assert "Our Brides" == resp_json["mainMenu"]["_json"][0]["blocks"][0]["child"][8]["name_en"]


def test_get_best_sellers():
    url = api_url + "/api/main/best-sellers"

    r = requests.get(url=url, headers=headers, cookies=cookies)

    assert 200 == r.status_code

    elems = ["id_product", "type", "product_name", "cost", "img", "_v", "categoryId", "name_attr", "slug",
             "productTags", "stockStatuses", "variations", "productLabels", "wishlistModule"]
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["rows"]:
            assert elem in elem1

