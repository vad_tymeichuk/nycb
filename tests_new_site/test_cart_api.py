from helpers.api.cart_helpers import *


def test_add_to_cart():
    r = add_to_cart()
    assert r.status_code == 201
    resp_json = r.json()
    print(resp_json)
    assert resp_json["addProductToCard"]["status"]

    if "item" in resp_json["addProductToCard"]:
        assert product_id == resp_json["addProductToCard"]["item"]["productsId"]
        assert "quantity" in resp_json["addProductToCard"]["item"]
    else:
        assert "product updated!" == resp_json["addProductToCard"]["msg"]

    delete_from_cart()


def test_delete_from_cart():
    add_to_cart()

    r = delete_from_cart()
    assert r.status_code == 200
    assert r.text == "OK"


def test_get_cart():
    clear_cart()

    add_to_cart()

    elems = ["items_cart", "coupons", "subTotal", "total", "couponsInfo"]
    r = get_cart()
    assert r.status_code == 200
    resp_json = r.json()
    print(r.text)
    for elem in elems:
        assert elem in resp_json
    assert product_id == resp_json["items_cart"][0]["productsId"]

    delete_from_cart()
