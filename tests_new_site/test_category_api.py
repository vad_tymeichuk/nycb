import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time
from helpers.api.product_helpers import get_parent_categories_tree_admin, get_parent_categories_tree

category_name = "API TEST" + date_time


def get_category_id():
    resp_json = get_parent_categories_tree_admin().json()
    for el in resp_json["tree"]:
        if el["title_category"] == category_name:
            return el["id_category"]


def create_category():
    url = api_url + "/api/categories/create"
    body = {
        "active": 1,
        "title_category_en": category_name,
        "title_category_fr": "TEST",
        "title_category_es": "TEST",
        "category_desc_en": "TEST",
        "category_desc_fr": "TEST",
        "category_desc_es": "TEST",
        "client_text_en": "TEST",
        "client_text_fr": "TEST",
        "client_text_es": "TEST",
        "seo_title_en": "TEST",
        "seo_title_fr": "TEST",
        "seo_title_es": "TEST",
        "seo_desc_en": "TEST",
        "seo_desc_fr": "TEST",
        "seo_desc_es": "TEST",
        "seo_key_en": "TEST",
        "seo_key_fr": "TEST",
        "seo_key_es": "TEST",
        "parentId": None,
        "hidden": 0,
        "slug": "test",
        "type": "product_category",
        "filters": {
            "inStock": 1,
            "sale": 1,
            "attributes": [{
                "attributeId": 46,
                "attributeTermIds": [610]
            }],
            "tags": [7]
        }
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def update_category(id_):
    category_name_new = category_name + date_time
    url = api_url + "/api/categories/update/" + str(id_)
    body = {
        "active": 1,
        "title_category_en": category_name_new
    }

    return requests.put(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def delete_category(id_):
    url = api_url + "/api/categories/update/" + str(id_)
    body = {
        "trash": True
    }

    return requests.put(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def test_get_parent_categories_tree_admin():
    category_elems = ["id_category", "title_category", "parentId", "slug", "type", "children"]
    r = get_parent_categories_tree_admin()
    resp_json = r.json()
    assert r.status_code == 200
    for i in range(0, len(resp_json["tree"])):
        for el in category_elems:
            assert el in resp_json["tree"][i]
            assert resp_json["tree"][i]["parentId"] is None
            assert resp_json["tree"][i]["type"] == "product_category"


def test_get_parent_categories_tree():
    category_elems = ["id_category", "title_category", "parentId", "slug", "type", "children"]
    r = get_parent_categories_tree()
    resp_json = r.json()
    assert r.status_code == 200
    for i in range(0, len(resp_json["tree"])):
        for el in category_elems:
            assert el in resp_json["tree"][i]
            assert resp_json["tree"][i]["parentId"] is None
            assert resp_json["tree"][i]["type"] == "product_category"


def test_create_category():
    r = create_category()

    assert r.status_code == 201
    resp_json = r.json()
    assert "Category created!" == resp_json["msg"]

    id_ = get_category_id()
    delete_category(id_)


def test_update_category():
    create_category()

    id_ = get_category_id()
    r = update_category(id_)
    assert r.status_code == 200
    resp_json = r.json()
    assert "Category updated!" == resp_json["msg"]

    delete_category(id_)


def test_delete_category():
    create_category()

    id_ = get_category_id()
    r = delete_category(id_)
    assert r.status_code == 200
    resp_json = r.json()
    assert "Category updated!" == resp_json["msg"]
