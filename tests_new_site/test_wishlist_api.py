import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies
from helpers.api.cart_helpers import get_prod_id_by_name

product_id = get_prod_id_by_name("josie")
product_id_1 = get_prod_id_by_name("emerald")
product_name = "Wedding dress Josie"
# wishlist_public_id = "XT7942FPVNBD"


def add_to_wishlist(prod_id):
    url = api_url + "/api/wishlist/add"

    data = {
        "productsId": prod_id,
        "attribute_variation": [
            {"attributeId": 1, "attributeTermId": 134},
            {"attributeId": 3, "attributeTermId": 10},
            {"attributeId": 13, "attributeTermId": 49}
        ],
        "custom_made": {
            "size": "s",
            "bust": "s",
            "waist": "s",
            "hips": "s",
            "sleeve_length": "s",
            "height": "s",
            "hollow_to_floor": "s",
            "armpit_to_floor": "s",
            "bicep": "s",
            "wrist": "s",
            "custom_size_notes": "s"
        }
    }

    return requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


# def get_wishlist_public_id():
#     resp_json = add_to_wishlist().json()
#
#     print(resp_json["addWishlist"])
#     return resp_json["addWishlist"]


def get_wishlist():
    url = api_url + "/api/wishlist"

    return requests.post(url=url, headers=headers, cookies=cookies)


def get_wishlist_id(prod_id):
    resp_json = get_wishlist().json()

    for elem in resp_json["wishlist"]:
        if elem["product"]["id_product"] == prod_id:
            return elem["id"]


def get_count(wishlist_public_id):
    url = api_url + "/api/wishlist/count"

    data = {
        "publicId": wishlist_public_id
    }

    return requests.get(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def delete_from_wishlist(wish_id):
    url = api_url + "/api/wishlist/" + str(wish_id)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def clear_wishlist():
    products_in_wishlist = []
    wishlist = get_wishlist().json()["wishlist"]
    if len(wishlist) > 0:
        for elem in get_wishlist().json()["wishlist"]:
            products_in_wishlist.append(elem["id"])

        for i in range(len(products_in_wishlist)):
            delete_from_wishlist(products_in_wishlist[i])
    else:
        print("wishlist is empty")


def test_get_count():

    clear_wishlist()

    wishlist_public_id = add_to_wishlist(product_id).json()["addWishlist"]

    r = get_count(wishlist_public_id)
    assert 200 == r.status_code
    resp_json = r.json()
    in_wishlist = resp_json["wishlistCount"]
    print(in_wishlist)
    #
    wishlist_public_id = add_to_wishlist(product_id_1).json()["addWishlist"]
    #
    r = get_count(wishlist_public_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["wishlistCount"] == in_wishlist + 1

    clear_wishlist()


def test_add_to_wishlist():
    r = add_to_wishlist(product_id)

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["addWishlist"] is not None

    delete_from_wishlist(get_wishlist_id(product_id))


def test_get_wishlist():
    add_to_wishlist(product_id)

    r = get_wishlist()
    assert 200 == r.status_code
    resp_json = r.json()
    for elem in resp_json["wishlist"]:
        if product_id == elem["product"]["id_product"]:
            assert product_name == elem["product"]["product_name"]

    delete_from_wishlist(get_wishlist_id(product_id))


def test_delete_from_wishlist():
    add_to_wishlist(product_id)

    r = delete_from_wishlist(get_wishlist_id(product_id))
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["wishlist"]
