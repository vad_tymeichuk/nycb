import json

import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time
from helpers.api.cart_helpers import *

coupon_code = "NYCBQATEST " + date_time
coupon_descr = "Test Coupon"
coupon_exp_date = "2022/04/11 00:00:00"
coupon_amount = 10
disc_type = "percentage"
product_id_1 = get_prod_id_by_name("josie")
product_id_2 = 963
coupon_code_upd = "NYCBQATEST NEW"
coupon_descr_upd = "Test Coupon NEW"
coupon_exp_date_upd = "2023/04/11 00:00:00"
coupon_amount_upd = coupon_amount + 1
disc_type_upd = "fixed cart"


def create_coupon():
    url = api_url + "/api/coupons/create"
    body = {
        "couponCode": coupon_code,
        "description": coupon_descr,
        "couponExpiryDate": coupon_exp_date,
        "couponAmount": str(coupon_amount),
        "discountType": disc_type,
        "allowFreeShipping": 1,
        "minimumSpend": "",
        "maximumSpend": "",
        "individualUseOnly": 1,
        "excludeSaleItems": 1,
        "selects": {
            "products": [product_id_1],
            "excludeProducts": [product_id_2],
            "productCategories": [49],
            "excludeCategories": [],
            "userEmail": ["root@root.root"]
        },
        "limitPerCoupon": "",
        "limitPerUser": "",
        "limitPerItems": None,
        "status": "active"
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def delete_coupon(coupon_id):
    url = api_url + "/api/coupons/delete/" + str(coupon_id)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def get_coupon_list():
    url = api_url + "/api/coupons/get-list"
    params = {"limit": 10, "page": 1}

    return requests.post(url=url, params=params, cookies=cookies)


def get_coupon_id_by_code():
    r = get_coupon_list()
    resp_json = r.json()
    for elem in resp_json["coupons"]:
        if coupon_code == elem["couponCode"]:
            return elem["id"]
        else:
            assert False, "Coupon id not found"


def update_coupon(coupon_id):
    url = api_url + "/api/coupons/update/" + str(coupon_id)
    body = {
        "couponCode": coupon_code_upd,
        "description": coupon_descr_upd,
        "couponExpiryDate": coupon_exp_date_upd,
        "couponAmount": str(coupon_amount_upd),
        "discountType": disc_type_upd,
        "allowFreeShipping": 1,
        "minimumSpend": "",
        "maximumSpend": "",
        "individualUseOnly": 1,
        "excludeSaleItems": 1,
        "selects": {
            "products": [product_id_1],
            "excludeProducts": [product_id_2],
            "productCategories": [11],
            "excludeCategories": [],
            "userEmail": ["root@root.root"]
        },
        "limitPerCoupon": None,
        "limitPerUser": None,
        "limitPerItems": None,
        "status": "active"
    }

    return requests.put(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def get_coupon_by_id(coupon_id):
    url = api_url + "/api/coupons/get-coupon/" + str(coupon_id)
    return requests.post(url=url, headers=headers, cookies=cookies)


def test_create_coupon():
    r = create_coupon()
    assert r.status_code == 201
    resp_json = r.json()
    assert resp_json["created"]
    assert resp_json["id"]
    assert resp_json["error"]["productsExclude"] is not None
    assert resp_json["error"]["excludeCategories"] is not None

    coupon_id = get_coupon_id_by_code()

    delete_coupon(coupon_id)


"""return type is not json"""


def test_update_coupon():
    create_coupon()
    print(get_coupon_id_by_code())
    coupon_id = get_coupon_id_by_code()

    r = update_coupon(coupon_id)
    assert r.status_code == 200
    assert "OK" == r.text

    r = delete_coupon(coupon_id)
    assert 200 == r.status_code


def test_get_coupon_by_id():
    create_coupon()

    coupon_id = get_coupon_id_by_code()

    r = get_coupon_by_id(coupon_id)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["coupon"]["id"] == coupon_id
    assert resp_json["coupon"]["couponCode"] == coupon_code
    assert coupon_exp_date[0:10] in resp_json["coupon"]["couponExpiryDate"]
    assert resp_json["coupon"]["description"] == coupon_descr
    assert resp_json["coupon"]["couponAmount"] == str(coupon_amount)
    assert resp_json["coupon"]["discountType"] == disc_type
    assert resp_json["coupon"]["excludeProducts"][0]["id_product"] == product_id_2
    assert resp_json["coupon"]["products"][0]["id_product"] == product_id_1

    delete_coupon(coupon_id)


"""return type is not json"""


def test_delete_coupon():
    create_coupon()

    coupon_id = get_coupon_id_by_code()

    r = delete_coupon(coupon_id)
    assert r.status_code == 200
    # resp_json = r.json()
    assert "OK" == r.text


def test_get_coupon_list():
    coupon_elems = ["id", "couponCode", "discountType", "couponAmount", "description", "couponExpiryDate", "status"]
    r = get_coupon_list()
    assert r.status_code == 200
    resp_json = r.json()
    for elem in coupon_elems:
        for elem1 in resp_json["coupons"]:
            assert elem in elem1


def test_get_restriction_values_for_products_and_categories():
    elems = ["id_category", "title_category_en"]
    url = api_url + "/api/coupons/get-restriction-values"
    body = {
        "restriction": "category",
        "restriction_excludes": []
    }
    r = requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["restriction_values"]:
            assert elem in elem1

    elems = ["id_product", "product_name_en"]
    body = {
        "restriction": "product",
        "restriction_excludes": []
    }
    r = requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["restriction_values"]:
            assert elem in elem1


def apply_coupon(coupon_code_):
    url = api_url + "/api/cart/add-coupon-to-cart"

    print(url)

    data = {
        "couponCode": coupon_code_
    }
    print("couponCode = ", data["couponCode"])

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def delete_coupon_from_cart(id_):
    url = api_url + "/api/cart/delete-coupon-to-cart/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def test_apply_coupon():
    clear_cart()

    add_to_cart()

    r = create_coupon()
    assert 201 == r.status_code

    coupon_id = get_coupon_id_by_code()
    print("\n test_apply_coupon, coupon_id =    ", coupon_id)

    r = apply_coupon(coupon_code)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["couponCart"]["status"]
    assert coupon_code in resp_json["couponCart"]["msg"]

    resp_json = get_cart().json()
    assert coupon_code == resp_json["couponsInfo"][0]["coupon"]
    if disc_type == "percentage":
        assert float(resp_json["subTotal"]) * coupon_amount / 100 == float(resp_json["couponsInfo"][0]["amount"])

    r = delete_coupon_from_cart(coupon_id)
    assert 200 == r.status_code
    assert "OK" == r.text

    resp_json = get_cart().json()
    assert 0 == len(resp_json["couponsInfo"])

    delete_coupon(coupon_id)

    delete_from_cart()