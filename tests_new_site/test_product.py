import json
import random
import uuid
import pytest
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time

prod_name = "Wedding dress APITEST " + date_time
prod_descr = "This product was created for testing purposes"
img_id = str(uuid.uuid1())
img_path = "/uploads/products/nycb-1627003052813.jpeg"
sku = random.randrange(1000, 9999)
gtin = random.randrange(100000000, 999999999)
price = random.randrange(500, 3000)
youtube_video = "https://youtu.be/BxV14h0kFs0"


def get_video_thumbnail():
    url = api_url + "/api/admin/product/get-video-thumb"

    data = {
        "link": youtube_video,
        "name": "youtube"
    }

    return requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies).json()["thumbnail"]


def create_product():
    url = api_url + "/api/admin/product/create"

    data = {
        "ship_class_id": 2,
        "product_name_en": prod_name,
        "product_name_fr": prod_name,
        "product_name_es": prod_name,
        "product_desc_en": prod_descr,
        "product_desc_fr": prod_descr,
        "product_desc_es": prod_descr,
        "product_short_desc_en": None,
        "product_short_desc_fr": None,
        "product_short_desc_es": None,
        "seo_title_en": "Wedding dress apitest",
        "seo_title_fr": "Wedding dress apitest",
        "seo_title_es": "Wedding dress apitest",
        "seo_desc_en": "Test Wedding dress apitest",
        "seo_desc_fr": "Test Wedding dress apitest",
        "seo_desc_es": "Test Wedding dress apitest",
        "seo_key_en": "",
        "seo_key_fr": "",
        "seo_key_es": "",
        "show": True,
        "images": [
            {
                "id": img_id,
                "fotoURL": img_path
            }
        ],
        "sku": sku,
        "gtin": gtin,
        "price": price,
        "tags": [9],
        "active": True,
        "partial_payment": {
            "partial": True,
            "custom": True
        },
        "video": [{
            "name": "youtube",
            "link": youtube_video,
            "thumbnail": get_video_thumbnail()
        }],
        "categoryId": 6,
        "attributes": [
            {
                "attributeId": 1,
                "variation": False,
                "terms": [2]
            },
            {
                "attributeId": 3,
                "variation": False,
                "terms": [9, 10]
            }]
    }

    r = requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies)
    id_ = r.json()["id"]

    return [r, id_]


def get_product_by_id(id_):
    url = api_url + "/api/admin/product/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_attribute_cost_by_product_slug():
    url = api_url + "/api/item/wedding-dress-josie-1/get-attributes-cost"

    data = {
        "product_attributes": [{
            "attributeId": 1,
            "attributeTermId": 134
        }, {
            "attributeId": 3,
            "attributeTermId": 10
        }, {
            "attributeId": 13,
            "attributeTermId": 49
        }]
    }
    return requests.get(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def delete_product(id_):
    url = api_url + "/api/admin/product/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def test_verify_get_product_limit_and_page():
    url = api_url + "/api/admin/product/all"
    data = {}
    params = {
        "page": 15,
        "limit": 10
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["page"] == 15
    assert resp_json["limit"] == 10


def test_verify_different_products_on_different_pages():
    # Get 3 products on the page 1
    url = api_url + "/api/admin/product/all"
    data = {}
    params = {
        "page": 1,
        "limit": 3
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    product1 = resp_json["rows"][0]["product_name"]
    product2 = resp_json["rows"][1]["product_name"]
    product3 = resp_json["rows"][2]["product_name"]
    # Get 3 products on the page 2
    params = {
        "page": 2,
        "limit": 3
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert product1 not in resp_json["rows"]
    assert product2 not in resp_json["rows"]
    assert product3 not in resp_json["rows"]
    # Make sure there are no products from page 1 on page 2
    product4 = resp_json["rows"][0]["product_name"]
    product5 = resp_json["rows"][1]["product_name"]
    product6 = resp_json["rows"][2]["product_name"]
    # Get page with all 6 products from page 1 and page 2
    params = {
        "page": 1,
        "limit": 6
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    # Make sure those products are the same - thus pagination works
    assert product1 in resp_json["rows"][0]["product_name"]
    assert product2 in resp_json["rows"][1]["product_name"]
    assert product3 in resp_json["rows"][2]["product_name"]
    assert product4 in resp_json["rows"][3]["product_name"]
    assert product5 in resp_json["rows"][4]["product_name"]
    assert product6 in resp_json["rows"][5]["product_name"]


def test_verify_default_response_values():
    url = api_url + "/api/admin/product/all"
    data = {}
    params = {
        "page": 29,
        "limit": 20
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["bulkActions"][0]["name"] == "update"
    assert resp_json["bulkActions"][1]["name"] == "delete"
    assert resp_json["stockStatuses"][0]["name"] == "In stock"
    assert resp_json["stockStatuses"][1]["name"] == "Out of stock"
    assert resp_json["stockStatuses"][2]["name"] == "Made to Order"
    assert resp_json["productStatuses"][0]["name"] == "productPublic"
    assert resp_json["productStatuses"][0]["count"] > 1
    assert resp_json["productStatuses"][1]["name"] == "productDraft"
    assert resp_json["productStatuses"][1]["count"] > 1
    assert resp_json["count"] == int(resp_json["productStatuses"][0]["count"]) \
           + int(resp_json["productStatuses"][1]["count"])
    assert resp_json["sorts"][0]["name"] == "ASC"
    assert resp_json["sorts"][1]["name"] == "DESC"
    assert resp_json["sorts"][1]["active"] == 1
    assert resp_json["types"][0]["name"] == "simple product"
    assert resp_json["types"][1]["name"] == "variable product"


def test_get_info_about_veil_product():
    url = api_url + "/api/admin/product/366"
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["product"]["product_name_en"] == "Wedding veil Portia"
    # the line below compares general price of the product
    assert resp_json["product"]["price"] is None
    assert resp_json["product"]["type"] == "variable product"
    assert resp_json["product"]["img"][0]["fotoURL"] == "/wp-content/uploads/2020/04/947-55_.jpg"
    assert resp_json["product"]["sku"] == "947"
    assert resp_json["product"]["gtin"] == "080101427388"
    assert resp_json["product"]["slug"] == "wedding-veil-portia-1"
    assert resp_json["product"]["stock_status"] == "Made to Order"
    assert resp_json["product"]["show"] is True
    assert resp_json["product"]["trash"] is False
    assert resp_json["product"]["ship_class_id"] == 3
    assert resp_json["product"]["categoryId"] == 5
    # assert resp_json["product"]["product_tags"][0]["name_tag"] == "Cathedral lace veil"
    assert "product_tags" in resp_json["product"]
    assert resp_json["product"]["pr_partial_payment"] is None


def test_get_info_about_wdress_product():
    url = api_url + "/api/admin/product/867"
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["product"]["product_name_en"] == "Wedding dress Josie"
    # the line below compares general price of the product
    assert resp_json["product"]["price"] == 0
    assert resp_json["product"]["type"] == "variable product"
    assert resp_json["product"]["img"][0]["fotoURL"] == "/wp-content/uploads/2021/01/Josie-1.jpg"
    assert resp_json["product"]["sku"] == "Josie"
    assert resp_json["product"]["gtin"] == "0709081296004"
    assert resp_json["product"]["slug"] == "wedding-dress-josie-1"
    assert resp_json["product"]["stock_status"] == "Made to Order"
    assert resp_json["product"]["show"] is True
    assert resp_json["product"]["trash"] is False
    assert resp_json["product"]["ship_class_id"] == 2
    assert resp_json["product"]["categoryId"] == 49
    assert resp_json["product"]["product_tags"][0]["name_tag"] == "wedding dress"
    assert "product_tags" in resp_json["product"]
    assert resp_json["product"]["pr_partial_payment"] is not None


@pytest.mark.skip(reason="temp replaced by the test below")
def test_get_attributes_of_particular_product():
    url = api_url + "/api/admin/product/2009/get-attributes"
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["attrGen"][0]["name"] == "Colors"
    assert resp_json["attrGen"][0]["product_terms"][0]["name"] == "Ivory"
    assert resp_json["attrGen"][0]["use_variation"] is True
    assert resp_json["attrGen"][0]["any"] is True
    assert resp_json["attrGen"][0]["use_attr_name"] is False
    assert resp_json["attrGen"][1]["name"] == "Size"
    assert resp_json["attrGen"][2]["name"] == "Train"
    assert resp_json["attrGen"][3]["name"] == "Brands"
    assert resp_json["attrGen"][4]["name"] == "Category Filter"
    assert resp_json["attrGen"][5]["name"] == "Dress Lenght"
    assert resp_json["attrGen"][6]["name"] == "Silhouette"
    assert resp_json["attrGen"][7]["name"] == "Style"
    assert resp_json["attrGen"][8]["name"] == "Fabric"
    assert resp_json["attrGen"][9]["name"] == "Detailing"
    assert resp_json["attrGen"][10]["name"] == "Neckline"
    assert resp_json["attrGen"][11]["name"] == "Back Clousure"
    assert resp_json["attrGen"][12]["name"] == "Sleeve Style"


def test_get_attributes_of_particular_product_1():
    url = api_url + "/api/admin/product/962/get-attributes"
    atr_elems = ["id", "name", "product_terms", "use_variation", "any", "use_attr_name"]
    terms_elems = ["id", "name"]
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()

    for elem1 in resp_json["attrGen"]:
        for elem in atr_elems:
            assert elem1[elem] is not None
            if elem == "product_terms":
                for elem2 in terms_elems:
                    for elem3 in elem1[elem]:
                        assert elem3[elem2] is not None


def test_get_variations_of_particular_product():
    url = api_url + "/api/admin/product/867/variation"
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["getVariation"][0]["enabled"] is True
    assert resp_json["getVariation"][0]["price"] == 850
    assert resp_json["getVariation"][0]["sale_price"] is None
    assert resp_json["getVariation"][0]["stock_status"] == "Made to order"
    assert resp_json["getVariation"][0]["gtin"] == "0709081296004"
    assert resp_json["getVariation"][0]["sku"] == "Josie"
    assert resp_json["getVariation"][0]["variation_product_throughs"][0]["type"] == "any"
    assert resp_json["getVariation"][0]["variation_product_throughs"][0]["attribute"] == "Colors"
    assert resp_json["getVariation"][0]["variation_product_throughs"][1]["type"] == "any"
    assert resp_json["getVariation"][0]["variation_product_throughs"][1]["attribute"] == "Size"
    assert resp_json["getVariation"][0]["variation_product_throughs"][2]["type"] == "any"
    assert resp_json["getVariation"][0]["variation_product_throughs"][2]["attribute"] == "Train"
    print(resp_json)


def test_get_product_search_list():
    url = api_url + "/api/admin/product/search-list"
    data = {}
    params = {
        "s": "Georgiana"
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["products"][0]["product_name"] == "Wedding dress Georgiana"
    assert resp_json["products"][1]["product_name"] == "Wedding veil Georgiana"


@pytest.mark.skip(reason="BUG - https://riseserv.atlassian.net/browse/NYCB-430")
def test_get_our_brides_images_list():
    """This test may fail.
    Make sure wedding-dress-tamie product has our brides images attached"""
    url = api_url + "/api/item/wedding-dress-tamie/get-reviews/images"
    data = {}
    params = {
        "page": 1,
        "limit": 10
    }
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    print(resp_json)
    assert "uploads" in resp_json["our_bride_review_images"]["rows"][0]["img_path"]
    assert int(resp_json["our_bride_review_images"]["rows"][0]["id"]) > 1


@pytest.mark.skip(reason="temp replaced by the test below")
def test_get_attributes():
    url = api_url + "/api/item/wedding-dress-tamie/get-attributes"
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    print(resp_json)
    assert resp_json["attributes"][0]["name"] == "Silhouette"
    assert resp_json["attributes"][0]["attribute_terms"][0]["name"] == "A-Line"
    assert resp_json["attributes"][3]["name"] == "Brands"
    assert resp_json["attributes"][3]["attribute_terms"][0]["name"] == "Silviamo"
    assert resp_json["attributes"][5]["name"] == "Detailing"
    assert resp_json["attributes"][5]["attribute_terms"][0]["name"] == "Guipure Lace"
    assert resp_json["attributes"][5]["attribute_terms"][1]["name"] == "Pearls"
    assert resp_json["attributes"][5]["attribute_terms"][2]["name"] == "Sequins"


def test_get_attributes_1():
    url = api_url + "/api/item/wedding-dress-tamie/get-attributes"
    atr_elems = ["id", "name", "attribute_terms"]
    terms_elems = ["id", "name"]
    data = {}
    params = {}
    r = requests.get(url=url, params=params, data=json.dumps(data), headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()

    for elem1 in resp_json["attributes"]:
        for elem in atr_elems:
            assert elem1[elem] is not None
            if elem == "attribute_terms":
                for elem2 in terms_elems:
                    for elem3 in elem1[elem]:
                        assert elem3[elem2] is not None


def test_create_delete_product():
    create_prod = create_product()

    assert 201 == create_prod[0].status_code
    resp_json = create_prod[0].json()
    assert resp_json["created"]
    prod_id = create_prod[1]

    r = delete_product(prod_id)

    assert 200 == r.status_code
    assert "OK" == r.text


def test_get_product_by_id():
    prod_id = create_product()[1]

    r = get_product_by_id(prod_id)

    assert 200 == r.status_code
    resp_json = r.json()
    assert prod_name == resp_json["product"]["product_name_en"]
    assert prod_descr == resp_json["product"]["product_desc_en"]
    assert str(sku) == resp_json["product"]["sku"]
    assert str(gtin) == resp_json["product"]["gtin"]
    assert price == resp_json["product"]["price"]

    delete_product(prod_id)


def test_get_attribute_cost_by_product_slug():
    # color - attr_id = 1
    # size - attr_id = 3
    # train - attr_id = 13
    r = get_attribute_cost_by_product_slug()
    assert 200 == r.status_code

    elems = ["12", "Cappuccino", "No train"]
    resp_json = r.json()
    assert resp_json["getCostProduct"]["shop"]
    assert resp_json["getCostProduct"]["cost"]["price"] is not None
    for elem in resp_json["getCostProduct"]["product_attributes"]:
        if elem["id"] == 3:
            assert "Size" == elem["name"]
        if elem["id"] == 1:
            assert "Colors" == elem["name"]
        if elem["id"] == 13:
            assert "Train" == elem["name"]
    for elem in resp_json["getCostProduct"]["activeAttributes"]:
        assert elem["name"] in elems
