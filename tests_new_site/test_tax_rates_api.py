import json

import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time

tax_rate_name = "Test rate! "

def get_tax_rates():
    url = api_url + "/api/tax-rates/all?limit=10&page=1"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_tax_rate_id():
    resp_json = get_tax_rates().json()

    for elem in resp_json["rows"]:
        if elem["tax_name"] == tax_rate_name:
            return elem["id"]


def create_tax_rate():
    url = api_url + "/api/tax-rates/create"

    data = {
        "country_code": "Ukraine",
        "city": "Rivne",
        "state_code": "Rivnenska oblast",
        "postcode": "33023",
        "priority": 1,
        "active": 1,
        "tax_name": tax_rate_name,
        "rate": 3
    }

    return requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def delete_tax_rate(_id):
    url = api_url + "/api/tax-rates/" + str(_id)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def test_get_rates():
    r = get_tax_rates()

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["rows"][0]["country_code"] is not None
    assert resp_json["rows"][0]["state_code"] is not None
    assert resp_json["rows"][0]["rate"] is not None
    assert resp_json["rows"][0]["tax_name"] is not None
    # assert resp_json["rows"][0]["country_codeId"] is not None
    # assert resp_json["rows"][0]["state_codeId"] is not None


def test_create_tax_rate():
    r = create_tax_rate()

    assert 201 == r.status_code
    # resp_json = r.json()
    assert "Created" == r.text

    tax_rate_id = get_tax_rate_id()

    delete_tax_rate(tax_rate_id)


def test_update_tax_rate():
    create_tax_rate()

    tax_rate_id = get_tax_rate_id()

    url = api_url + "/api/tax-rates/" + str(tax_rate_id)

    data = {
        "country_code": "UA",
        "city": "Kyiv",
        "state_code": "223",
        "postcode": "01001",
        "priority": 1,
        "active": 1,
        "tax_name": tax_rate_name + date_time,
        "rate": 3
    }

    r = requests.put(url=url, data=json.dumps(data), headers=headers, cookies=cookies)

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["update"]
    assert resp_json["msg"] == "Tax updated!"

    delete_tax_rate(tax_rate_id)


def test_delete_tax_rate():
    create_tax_rate()
    tax_rate_id = get_tax_rate_id()

    url = api_url + "/api/tax-rates/" + str(tax_rate_id)
    r = requests.delete(url=url, headers=headers, cookies=cookies)

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["deleted"]
