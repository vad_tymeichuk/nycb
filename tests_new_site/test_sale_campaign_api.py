import json

import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_, date_time, plus_one_day

campaign_id = 103
type_condition = 3
search_value = "dress"
timer_label = "This is good Sale!"
sale_camp_name = "TEST API " + date_time
disc_amount = "100"
disc_amount_new = "200"
from_ = "500"
to = "200"
from_new = "600"
to_new = "1500"
start_date = date_
end_date = plus_one_day(date_)


def create_sale_campaign():
    url = api_url + "/api/sale-campaign/create"

    data = {
        "name": sale_camp_name,
        "start": {
            "date": start_date,
            "time": "00:01 AM"
        },
        "end": {
            "date": end_date,
            "time": "00:01 AM"
        },
        "discount": {
            "discount_status": 1,
            "discount_priority": 2,
            "discount_mode_value": [
                {
                    "from": from_,
                    "to": to,
                    "amount": disc_amount
                }
            ],
            "scDiscountTypeId": 3
        },
        "countDownTimer_status": 1,
        "countDownTimer_label": timer_label,
        "sticky_header": {
            "status": True,
            "sticky_mobile": True,
            "sticky_desktop": True,
            "headline": "",
            "sub_headline": "",
            "countdown_timer": True,
            "time_show": ""
        },
        "sticky_footer": {
            "status": True,
            "sticky_mobile": True,
            "stucky_desktop": True,
            "headline": "",
            "sub_headline": "",
            "countdown_timer": True,
            "time_show": ""
        },
        "advanced_sticky_show": 12,
        "rules": [
            [
                {
                    "condition": 3,
                    "type_being": 1,
                    "products": [962]
                }
            ]
        ]
    }
    resp_json = requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies).json()

    return [resp_json, resp_json["saleCampaignId"]]


def update_sale_campaign(id_):
    url = api_url + "/api/sale-campaign/" + str(id_)

    data = {
        "discount": {
            "discount_status": 1,
            "discount_priority": 2,
            "discount_mode_value": [
                {
                    "from": from_new,
                    "to": to_new,
                    "amount": disc_amount_new
                }
            ],
            "scDiscountTypeId": 3
        }
    }

    return requests.put(url=url, data=json.dumps(data), headers=headers, cookies=cookies)


def delete_sale_campaign(id_):
    url = api_url + "/api/sale-campaign/delete/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def get_all_campaigns():
    url = api_url + "/api/sale-campaign/admin/get-all"
    params = {"page": 1,
              "limit": 24
              }

    return requests.get(url=url, params=params, headers=headers, cookies=cookies)


def get_rules_for_campaign(campaign_id_):
    url = api_url + "/api/sale-campaign/get-admin/" + str(campaign_id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_conditions():
    url = api_url + "/api/sale-campaign/get-conditions"

    return requests.post(url=url, headers=headers, cookies=cookies)


def get_discount_types():
    url = api_url + "/api/sale-campaign/get-discount-types"

    return requests.post(url=url, headers=headers, cookies=cookies)


def get_condition_values():
    url = api_url + "/api/sale-campaign/get-condition-values"
    body = {
        "type_condition": type_condition,
        "search_value": search_value
    }

    return requests.post(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def test_get_all_campaigns():
    r = get_all_campaigns()
    elems = ["id", "name", "start_time", "end_time", "active", "discount_priority"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["rows"]:
            assert elem in elem1


def test_create_delete_sale_campaign():
    r = create_sale_campaign()
    resp_json = r[0]
    assert resp_json["created"]

    sale_camp_id = r[1]

    r = delete_sale_campaign(sale_camp_id)
    assert 200 == r.status_code
    assert "OK" == r.text


def test_update_sale_campaign():
    sale_camp_id = create_sale_campaign()[1]

    r = update_sale_campaign(sale_camp_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["updated"]

    r = get_rules_for_campaign(sale_camp_id)

    assert 200 == r.status_code
    resp_json = r.json()

    assert from_new == resp_json["sale_campaign"]["discount_mode_value"][0]["from"]
    assert disc_amount_new == resp_json["sale_campaign"]["discount_mode_value"][0]["amount"]
    assert to_new == resp_json["sale_campaign"]["discount_mode_value"][0]["to"]

    delete_sale_campaign(sale_camp_id)


def test_get_rules_for_campaign():
    sale_camp_id = create_sale_campaign()[1]

    r = get_rules_for_campaign(sale_camp_id)

    elems = ["id", "name", "start_time", "end_time", "discount_status", "discount_priority", "discount_mode_value",
             "countDownTimer_status", "countDownTimer_label", "sticky_header", "sticky_footer", "advanced_sticky_show",
             "active", "date_active", "createdAt", "updatedAt", "deletedAt", "scDiscountTypeId", "sc_rules",
             "sc_discount_type", "start_date", "end_date"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        assert elem in resp_json["sale_campaign"]
    assert sale_camp_name == resp_json["sale_campaign"]["name"]
    assert disc_amount == resp_json["sale_campaign"]["discount_mode_value"][0]["amount"]
    assert resp_json["sale_campaign"]["active"]
    assert timer_label == resp_json["sale_campaign"]["countDownTimer_label"]

    delete_sale_campaign(sale_camp_id)


def test_get_conditions():
    r = get_conditions()
    elems = ["id", "name", "type", "active"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["conditions"]:
            assert elem in elem1


def test_get_discount_types():
    r = get_discount_types()
    elems = ["id", "name", "active"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["discount_types"]:
            assert elem in elem1


def test_get_conditions_values():
    r = get_condition_values()

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in resp_json["searchValue"]:
        assert "id_product" in elem
        assert search_value in elem["product_name"]
