import json

import requests
from helpers.api.common_helpers import api_url, headers, cookies

partial_amount = 31


def get_partial_payment():
    url = api_url + "/api/admin/partial-payment"
    return requests.get(url=url, headers=headers, cookies=cookies)


def update_partial_payment():
    global partial_amount
    url = api_url + "/api/admin/partial-payment"
    data = {"partial_amount": partial_amount}
    return requests.put(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def test_update_partial_payment():
    global partial_amount
    partial_amount = 32
    try:
        r = update_partial_payment()
        assert r.status_code == 200
        resp_json = r.json()
        assert resp_json["partial_payment"][0] == 1 or resp_json["partial_payment"][0] == 0
    finally:
        partial_amount = 30
        update_partial_payment()


def test_get_partial_payment():
    global partial_amount
    update_partial_payment()
    try:
        r = get_partial_payment()
        assert r.status_code == 200
        resp_json = r.json()
        assert resp_json["partial_payment"]["id"] == 1
        assert resp_json["partial_payment"]["partial_amount"] == partial_amount
    finally:
        partial_amount = 30
        update_partial_payment()
