import pytest

from helpers.api.cart_helpers import *
import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies, date_time
from helpers.api.cart_helpers import add_to_cart, delete_from_cart, product_id, get_ship_cost_for_case_id, custom_made
from helpers.api.product_helpers import get_product_price

prod_id = product_id
prod_price = get_product_price(prod_id)
country = "US"
order_notes = "TEST ORDER"
order_notes_upd = order_notes + date_time
city = "Berkeley"
street = "1812 Parker Street"
street_upd = street + " " + date_time
ZIP = "94711"
email = "nycb-qa-test@riseserv.com"
email_upd = email + " " + date_time
state = "CA"
shipCaseActiveId = 725  # 695 - Free, 725 - Express - $ 79
fname = "Riseservice"
lname = "Tester"
lname_upd = "Developer"
track_number = "12345678"
track_number_upd = "123456789"
date_shipped = date_time
delivery_srvc_id = 1
status_shipped = "Pending"
status_shipped_upd = "Shipped"
# payment_method = ["stripeMethod", "paypalMethod"]
payment_method = random.choice(["stripeMethod", "paypalMethod"])
custom_made_percentage = 12


def create_order():
    url = api_url + "/api/cart/checkout"
    data = {
        "billing_address": {
            "first_name": fname,
            "last_name": lname,
            "company": "RiseServ",
            "country": country,
            "state_country": state,
            "town_city": city,
            "address": street,
            "address_opt": "",
            "postcode_zip": ZIP,
            "email_address": email,
            "phone": "5103265840",
            "date_of_wedding": "23.04.2025"
        },
        "shipping_address": {
            "first_name": fname,
            "last_name": lname,
            "company": "RiseServ",
            "country": country,
            "state_country": state,
            "town_city": city,
            "address": street,
            "address_opt": "",
            "postcode_zip": ZIP
        },
        "order_notes": order_notes,
        "geo": {
            "countryCode": "US",
            "country_Id": 226,
            "city": city,
            "stateCode": "",
            "postCodeZip": ZIP,
            "shipCaseActiveId": shipCaseActiveId
        },
        "payMethod": {
            "type": payment_method,
            "data": {}
        }
    }

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def update_order(id_):
    url = api_url + "/api/admin/orders/update/" + str(id_)
    data = {
        "billing_adress": {
            "first_name": fname,
            "last_name": lname_upd,
            "company": "RiseServ",
            "country": country,
            "state_country": state,
            "town_city": city,
            "address": street_upd,
            "address_opt": "",
            "postcode_zip": ZIP,
            "email_address": email_upd,
            "phone": "5103265840",
            "date_of_wedding": "23.04.2025"
        },
        "shipping_adress": {
            "first_name": fname,
            "last_name": lname_upd,
            "company": "RiseServ",
            "country": country,
            "state_country": state,
            "town_city": city,
            "address": street,
            "address_opt": "",
            "postcode_zip": ZIP
        },
        "order_notes": order_notes_upd,
        "geo": {
            "countryCode": "US",
            "country_Id": 226,
            "city": city,
            "stateCode": "",
            "postCodeZip": ZIP,
            "shipCaseActiveId": shipCaseActiveId
        },
        "payMethod": {
            "type": random.choice(payment_method),
            "data": {}
        }
    }

    return requests.put(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def get_all_orders():
    url = api_url + "/api/admin/orders/get-all"
    params = {"statusID": 1,
              "page": 1,
              "limit": 5
              }

    return requests.get(url=url, headers=headers, params=params, cookies=cookies)


def get_order_id(create_order_json):
    order_number = create_order_json["order_number"]

    for elem in get_all_orders().json()["getOrders"]:
        if order_number == elem["order_number"]:
            return elem["id"]

    return None


def get_order_by_id(id_):
    url = api_url + "/api/admin/orders/get/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def delete_order(id_):
    url = api_url + "/api/admin/orders/action"
    data = {
        "orders": [id_],
        "actions": "delete"
    }

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def add_shipping(order_id_):
    url = api_url + "/api/admin/orders/add-shipping"

    data = {
        "tracking_number": track_number,
        "date_shipped": str(date_shipped),
        "status_shipped": status_shipped,
        "delivery_serviceId": delivery_srvc_id,
        "orderId": order_id_
    }

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def update_ship_status(track_id_, order_id_):
    url = api_url + "/api/admin/orders/shipping/" + str(track_id_)

    data = {
        "tracking_number": track_number_upd,
        "date_shipped": str(date_shipped),
        "status_shipped": status_shipped_upd,
        "delivery_serviceId": delivery_srvc_id,
        "orderId": order_id_
    }

    return requests.put(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def get_list_of_delivery_services():
    url = api_url + "/api/admin/delivery-service/get-list"

    return requests.get(url=url, headers=headers, cookies=cookies)


def send_reminder(id_):
    url = api_url + "/api/admin/orders/send-reminder/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def gen_receipt(id_):
    url = api_url + "/api/admin/orders/gen-receipt/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


# @pytest.mark.skip(reason="fails due to 404 error")
def test_get_all_orders():
    r = get_all_orders()

    assert 200 == r.status_code
    resp_json = r.json()
    assert "possibleActions" in resp_json
    assert "dateFilter" in resp_json
    assert "orderStatuses" in resp_json
    for elem in resp_json["getOrders"]:
        assert elem["totalAmount"] is not None
        assert "orderStatusId" in elem
        assert "shipping_address" in elem
        assert "billing_address" in elem


# @pytest.mark.skip(reason="fails due to 404 error")
def test_create_delete_order():
    clear_cart()

    add_to_cart()

    r = create_order()

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["success"]
    assert resp_json["order_number"] is not None
    assert payment_method == resp_json["payMethod"]["type"]

    r = delete_order(get_order_id(resp_json))

    assert 200 == r.status_code
    assert "OK" == r.text


def test_get_order_by_id():
    clear_cart()

    add_to_cart()

    r = create_order()

    ord_id = get_order_id(r.json())

    r = get_order_by_id(ord_id)

    assert 200 == r.status_code
    resp_json = r.json()
    try:
        if custom_made is None:
            assert prod_price + get_ship_cost_for_case_id(shipCaseActiveId) == float(resp_json["totalAmount"])
        else:
            price_w_custom_made = prod_price*(1 + custom_made_percentage/100) + get_ship_cost_for_case_id(shipCaseActiveId)
            assert round(price_w_custom_made, 2) == float(resp_json["totalAmount"])
        assert order_notes == resp_json["order_note"]
        assert product_id == resp_json["orderItems"][0]["additionalInformation"]["id"]
        assert product_id == resp_json["orderItems"][0]["productsId"]
        assert country == resp_json["billing_address"]["country"]
        assert street == resp_json["billing_address"]["address"]
        assert city == resp_json["billing_address"]["town"]
        assert ZIP == resp_json["billing_address"]["postcode"]
        assert email == resp_json["billing_address"]["email_address"]
        assert country == resp_json["shipping_address"]["country"]
        assert street == resp_json["shipping_address"]["address"]
        assert city == resp_json["shipping_address"]["town"]
        assert ZIP == resp_json["shipping_address"]["postcode"]
    finally:
        delete_from_cart()

        delete_order(ord_id)


def test_get_list_of_delivery_services():
    r = get_list_of_delivery_services()

    list_of_del_serv = ["USPS", "Aramex", "Deutsche Post DHL", "Ukrposhta", "UPS", "Ukraine EMS", "DHL Parcel", "Fedex"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in resp_json["deliveryServiceObject"]:
        assert elem["name"] in list_of_del_serv


# @pytest.mark.skip(reason="fails due to 404 error - https://riseserv.atlassian.net/browse/NYCB-958")
def test_update_order():
    clear_cart()
    add_to_cart()

    ord_id = get_order_id(create_order().json())

    r = update_order(ord_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["updateOrder"]["updated"]
    elems = ["checkOrder", "updateOrder", "billingAddress", "shippingAddress"]
    for elem in resp_json["updateOrder"]["result"]:
        assert elem["name"] in elems
        assert elem["status"]

    delete_order(ord_id)

    delete_from_cart()


# @pytest.mark.skip(reason="fails due to 404 error - https://riseserv.atlassian.net/browse/NYCB-958")
def test_add_update_shipping():
    clear_cart()

    add_to_cart()

    ord_id = get_order_id(create_order().json())

    r = add_shipping(ord_id)
    assert 201 == r.status_code
    resp_json = r.json()
    assert track_number == resp_json["shippingTrack"]["tracking_number"]
    assert date_shipped == resp_json["shippingTrack"]["date_shipped"]
    assert delivery_srvc_id == resp_json["shippingTrack"]["delivery_serviceId"]
    assert status_shipped == resp_json["shippingTrack"]["status_shipped"]
    track_id = resp_json["shippingTrack"]["id"]

    r = get_order_by_id(ord_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert track_number == resp_json["shipped_status"]["tracking_number"]
    assert date_shipped == resp_json["shipped_status"]["date_shipped"]
    assert delivery_srvc_id == resp_json["shipped_status"]["delivery_serviceId"]
    assert status_shipped == resp_json["shipped_status"]["status_shipped"]

    r = update_ship_status(track_id, ord_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["update"]

    resp_json_1 = get_order_by_id(ord_id).json()
    assert track_number_upd == resp_json_1["shipped_status"]["tracking_number"]
    assert status_shipped_upd == resp_json_1["shipped_status"]["status_shipped"]

    delete_order(ord_id)

    delete_from_cart()


# @pytest.mark.skip(reason="fails due to 404 error - https://riseserv.atlassian.net/browse/NYCB-958")
def test_reminder_and_receipt():
    clear_cart()

    add_to_cart()

    ord_id = get_order_id(create_order().json())

    r = send_reminder(ord_id)
    assert 200 == r.status_code
    assert "OK" == r.text

    # r = gen_receipt(ord_id)
    # assert 200 == r.status_code
    # resp_json = r.json()
    # assert resp_json["gen"]
    # assert "pdf" and "receipt" in resp_json["receipt_link"]

    delete_order(ord_id)

    clear_cart()
