from helpers.api.user_mgt_helpers import *


def test_login():
    r = login()
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["login"] is True
    assert resp_json["quantitybar"]["wish_quantity"] is not None
    assert resp_json["quantitybar"]["cart_quantity"] is not None


def test_get_current_user():
    url = api_url + "/api/users/curentuser"
    print(test_api_user_password)
    r = requests.get(url=url, headers=headers,
                     cookies=login_get_cookies(test_api_user_email, test_api_user_password))
    assert r.status_code == 200
    resp_json = r.json()
    assert resp_json["curentUser"]["name"] == "Riseservise"
    assert "testuser" in resp_json["curentUser"]["username"]
    assert resp_json["curentUser"]["email"] == test_api_user_email
    assert resp_json["curentUser"]["img_path"] == ""
    # assert resp_json["quantitybar"]["wish_quantity"] is not None
    # assert resp_json["quantitybar"]["cart_quantity"] is not None


def test_get_roles():
    url = api_url + "/api/roles/get-all"

    r = requests.post(url=url, headers=headers, cookies=cookies)

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in resp_json:
        assert "id" and "name" in elem


def test_get_user_info():
    """returns info about root user with id = 4 """
    url = api_url + "/api/admin/users/169"

    r = requests.post(url=url, headers=headers, cookies=cookies)

    assert 200 == r.status_code
    resp_json = r.json()
    assert "riseservice tester" == resp_json["name"]
    assert "Tester" == resp_json["last_name"]
    assert "qa-tester" == resp_json["username"]
    assert "nycb20@riseservice.com.ua" == resp_json["email"]
    assert "admin" == resp_json["role"]["name"]
