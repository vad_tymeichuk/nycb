import json

import pytest
import requests
from helpers.api.common_helpers import api_url, headers, cookies
from helpers.api.upload_image_helpers import upload_image, get_image_id, delete_image, upload_several_images, get_image_ids

upload_path = "/api/our-brides/uploads"


def show_img_in_our_brides_by_id(image_id):
    url = api_url + "/api/our-brides/update/" + str(image_id)
    body = {
        "our_bride_show": True,
    }

    return requests.put(url=url, data=json.dumps(body), headers=headers, cookies=cookies)


def get_all_our_brides_photos():
    url = api_url + "/api/our-brides/get-list?limit=25&page=1"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_all_photo_ids():
    ids = []
    r = get_all_our_brides_photos()
    resp_json = r.json()
    for elem in resp_json["our_brides_images"]["rows"]:
        ids.append(elem["id"])

    return ids


def get_photo_info_by_id(id_):
    # id_ = random.choice(get_all_photo_ids())
    url = api_url + "/api/our-brides/get-photo/" + str(id_)

    return requests.post(url=url, headers=headers, cookies=cookies)


@pytest.mark.skip(reason="waiting for staging server update")
def test_get_all_our_brides_photos():
    elems = ["id", "img_path", "title", "createdAt"]

    r = get_all_our_brides_photos()
    assert r.status_code == 200
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["our_brides_images"]["rows"]:
            assert elem in elem1


def test_get_photo_info_by_id():
    r = upload_image(upload_path)
    img_id = get_image_id(r.json())

    r = get_photo_info_by_id(img_id)
    try:
        assert r.status_code == 200
        resp_json = r.json()
        assert "our-bride" == resp_json["image"]["type"]
        assert "id" in resp_json["image"]
        assert "title" in resp_json["image"]
        # assert "type" in resp_json["image"]
        assert "our_bride_show" in resp_json["image"]
        assert "product_show" in resp_json["image"]
        assert "img_path" in resp_json["image"]
        assert "product_link" in resp_json["image"]
        assert "trash" in resp_json["image"]
        assert "createdAt" in resp_json["image"]
        assert "deletedAt" in resp_json["image"]
        # assert "productsId" in resp_json["image"]
        assert "rReviewId" in resp_json["image"]
        assert "products" in resp_json["image"]
    finally:
        delete_image(img_id)


def test_delete_image():
    img_id = get_image_id(upload_image(upload_path).json())

    r = delete_image(img_id)
    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["delete"]


@pytest.mark.skip(reason="waiting for staging server update")
def test_get_img_in_our_brides_list():
    img_id = get_image_id(upload_image(upload_path).json())
    print("Image id ", img_id)
    show_img_in_our_brides_by_id(img_id)

    try:
        r = get_all_our_brides_photos()
        assert 200 == r.status_code
        resp_json = r.json()
        for elem in resp_json["our_brides_images"]["rows"]:
            if img_id == elem["id"]:
                assert True
            else:
                assert False
    finally:
        delete_image(img_id)


def test_upload_several_images():
    r = upload_several_images(upload_path)
    assert 200 == r.status_code
    resp_json = r.json()
    img_ids = get_image_ids(resp_json)
    try:
        for elem in resp_json["images"]:
            assert "id" in elem
            assert "img_path" in elem
    finally:
        for n in range(0, 2):
            delete_image(img_ids[n])
