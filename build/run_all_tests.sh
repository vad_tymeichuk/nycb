#!/bin/sh
export REPORT_OUTPUT="report.html"
#Specify your email password for local container testing only
#Don't forget to remove this variable declaration from scripts/run_all_tests.sh
# before pushing your image to the cloud!
#export MAIL_PASS=<your_email_password_here>

# Run tests
pytest ./tests --html=${REPORT_OUTPUT} --self-contained-html
# Send results
sleep 10
python ./notification_workers/send_notifications.py

