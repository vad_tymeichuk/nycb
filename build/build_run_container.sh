#!/bin/sh
cp -f build/run_all_tests.sh scripts/run_all_tests.sh

find . | grep -E "(__pycache__|__pytest_cache__|.pytest_cache|\.pyc|\.pyo$)" | xargs rm -rf

docker build . -t registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test -f Dockerfile

docker container run registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test