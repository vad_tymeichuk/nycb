#!/bin/sh
find . | grep -E "(__pycache__|__pytest_cache__|.pytest_cache|\.pyc|\.pyo$)" | xargs rm -rf

docker build . -t registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test -f Dockerfile

docker push registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test