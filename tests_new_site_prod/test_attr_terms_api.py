# import json
# import requests
# from helpers.api.common_helpers import api_url, headers, cookies
#
# attr_name = "test"
# term_name = "term-test"
#
#
# def get_all_attr():
#     url = api_url + "/api/attributes/get-attributes-list"
#     return requests.post(url=url, headers=headers, cookies=cookies)
#
#
# def get_attr_id():
#     attr_id = None
#     r = get_all_attr()
#     attr_list = r.json()
#     for attr in attr_list["attributes"]:
#         if attr_name == attr["name"]:
#             attr_id = attr["id"]
#             return attr_id
#     return attr_id
#
#
# def get_all_terms():
#     url = api_url + "/api/attribute-terms/get-terms-list"
#     return requests.post(url=url, headers=headers, cookies=cookies)
#
#
# def get_term_id():
#     term_id = None
#     r = get_all_terms()
#     term_list = r.json()
#     for term in term_list["attributes_terms"]:
#         if term_name == term["name"]:
#             term_id = term["id"]
#             return term_id
#     return term_id
#
#
# def create_attr():
#     url = api_url + "/api/attributes/create"
#     body = {
#         "name_en": attr_name,
#         "name_fr": attr_name,
#         "name_es": attr_name,
#         "slug": "",
#         "order_by": "1"
#     }
#     return requests.post(url=url, data=json.dumps(body), headers=headers, cookies=cookies)
#
#
# def delete_attr():
#     attr_id = get_attr_id()
#     url = api_url + "/api/attributes/delete/" + str(attr_id)
#     return requests.delete(url=url, headers=headers, cookies=cookies)
#
#
# def create_term():
#     create_attr()
#
#     url = api_url + "/api/attribute-terms/create"
#     attr_id = get_attr_id()
#     body = {
#         "name_en": term_name,
#         "name_fr": term_name,
#         "name_es": term_name,
#         "slug": "",
#         "order_by": "1",
#         "attributeId": attr_id
#     }
#     return requests.post(url=url, data=json.dumps(body), headers=headers, cookies=cookies)
#
#
# def delete_term():
#     term_id = get_term_id()
#     url = api_url + "/api/attribute-terms/delete/" + str(term_id)
#     return requests.delete(url=url, headers=headers, cookies=cookies)
#
#
# def test_get_attr_list():
#     r = get_all_attr()
#     res_json = r.json()
#     for el in res_json["attributes"]:
#         assert "id" in el
#         assert "name" in el
#
#
# def test_get_attr_by_id():
#     create_attr()
#
#     attr_id = get_attr_id()
#     url = api_url + "/api/attributes/get-attribute/" + str(attr_id)
#     r = requests.post(url=url, headers=headers, cookies=cookies)
#     resp_json = r.json()
#     assert resp_json["attribute"]["id"] == attr_id
#     assert "attribute_terms" in resp_json["attribute"]
#     assert resp_json["attribute"]["name"] == attr_name
#
#     delete_attr()
#
#
# def test_get_terms_list():
#     r = get_all_terms()
#     resp_json = r.json()
#     for el in resp_json["attributes_terms"]:
#         assert "id" in el
#         assert "name" in el
#
#
# def test_get_term_by_id():
#     create_term()
#
#     term_id = get_term_id()
#     url = api_url + "/api/attribute-terms/get-term/" + str(term_id)
#     r = requests.post(url=url, headers=headers, cookies=cookies)
#     resp_json = r.json()
#     assert resp_json["attribute"]["id"] == term_id
#     assert resp_json["attribute"]["name_en"] == term_name
#     assert resp_json["attribute"]["name_fr"] == term_name
#     assert resp_json["attribute"]["name_es"] == term_name
#     assert resp_json["attribute"]["attributeId"] == get_attr_id()
#
#     delete_term()
#     delete_attr()
#
#
# def test_create_attr():
#     r = create_attr()
#     resp_json = r.json()
#     assert r.status_code == 201
#     assert resp_json["msg"] == "Created!"
#
#     delete_attr()
#
#
# def test_update_attr():
#     create_attr()
#
#     attr_id = get_attr_id()
#     url = api_url + "/api/attributes/update/" + str(attr_id)
#     params = {
#         "name_en": attr_name + " updated1",
#         "name_fr": attr_name + " updated1",
#         "name_es": attr_name + " updated1",
#         "slug": "",
#         "order_by": "1"
#     }
#     r = requests.put(url=url, params=params, headers=headers, cookies=cookies)
#     resp_json = r.json()
#     assert r.status_code == 200, resp_json
#     assert resp_json["msg"] == "updated!"
#
#     delete_attr()
#
#
# def test_delete_attr():
#     create_attr()
#     r = delete_attr()
#     resp_json = r.json()
#     assert r.status_code == 200
#     assert resp_json["delete"] is True
#
#
# def test_create_term():
#     r = create_term()
#     resp_json = r.json()
#     assert r.status_code == 201
#     assert resp_json["msg"] == "Created!"
#
#     delete_term()
#     delete_attr()
#
#
# def test_update_term():
#     create_term()
#
#     term_id = get_term_id()
#     attr_id = get_attr_id()
#     url = api_url + "/api/attribute-terms/update/" + str(term_id)
#     params = {
#         "name_en": term_name + " updated1",
#         "name_fr": term_name + " updated1",
#         "name_es": term_name + " updated1",
#         "slug": "",
#         "order_by": "1",
#         "attributeId": attr_id
#     }
#     r = requests.put(url=url, params=params, headers=headers, cookies=cookies)
#     resp_json = r.json()
#     assert r.status_code == 200, resp_json
#     assert resp_json["msg"] == "updated!"
#
#     delete_term()
#     delete_attr()
#
#
# def test_delete_term():
#     create_term()
#
#     r = delete_term()
#     resp_json = r.json()
#     assert r.status_code == 200
#     assert resp_json["delete"] is True
#
#     delete_attr()
