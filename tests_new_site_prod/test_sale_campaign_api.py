import requests
from helpers.api.common_helpers import api_url, headers, cookies

campaign_id = 22
type_condition = 3
search_value = "dress"


def get_all_campaigns():
    url = api_url + "/api/sale-campaign/admin/get-all"
    params = {"page": 1,
              "limit": 24
              }
    print(url)

    return requests.get(url=url, params=params, headers=headers, cookies=cookies)


def get_rules_for_campaign():
    url = api_url + "/api/sale-campaign/get-admin/" + str(campaign_id)

    return requests.get(url=url, headers=headers, cookies=cookies)


def test_get_all_campaigns():
    r = get_all_campaigns()
    elems = ["id", "name", "start_time", "end_time", "active", "discount_priority"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        for elem1 in resp_json["rows"]:
            assert elem in elem1


def test_get_rules_for_campaign():
    r = get_rules_for_campaign()
    elems = ["id", "name", "start_time", "end_time", "discount_status", "discount_priority", "discount_mode_value",
             "countDownTimer_status", "countDownTimer_label", "sticky_header", "sticky_footer", "advanced_sticky_show",
             "active", "date_active", "createdAt", "updatedAt", "deletedAt", "scDiscountTypeId", "sc_rules",
             "sc_discount_type", "start_date", "end_date"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in elems:
        assert elem in resp_json["sale_campaign"]
    assert "Sale Evening Dresses" == resp_json["sale_campaign"]["name"]
    # assert 19 == resp_json["sale_campaign"]["discount_mode_value"]
    assert resp_json["sale_campaign"]["active"]