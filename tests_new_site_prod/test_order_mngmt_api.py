import pytest

from helpers.api.cart_helpers import *
import json
import requests
from helpers.api.common_helpers import api_url, headers, cookies
from helpers.api.cart_helpers import add_to_cart, delete_from_cart, product_id
from datetime import datetime

date_time = datetime.now().strftime("%m/%d/%Y-%H:%M:%S")

country = "US"
order_notes = "TEST ORDER"
order_notes_upd = order_notes + date_time
city = "Berkeley"
street = "1812 Parker Street"
ZIP = "94711"
email = "nycb-qa-test@riseserv.com"
state = "CA"


def get_all_orders():
    url = api_url + "/api/admin/orders/get-all"
    params = {"statusID": 1,
              "page": 1,
              "limit": 5
              }

    return requests.get(url=url, headers=headers, params=params, cookies=cookies)


def get_order_by_id(id_):
    url = api_url + "/api/admin/orders/get/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def delete_order(id_):
    url = api_url + "/api/admin/orders/action"
    data = {
        "orders": [id_],
        "actions": "delete"
    }

    return requests.post(url=url, headers=headers, data=json.dumps(data), cookies=cookies)


def get_list_of_delivery_services():
    url = api_url + "/api/admin/delivery-service/get-list"

    return requests.get(url=url, headers=headers, cookies=cookies)


def send_reminder(id_):
    url = api_url + "/api/admin/orders/send-reminder/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def test_get_all_orders():
    r = get_all_orders()

    assert 200 == r.status_code
    resp_json = r.json()
    assert "possibleActions" in resp_json
    assert "dateFilter" in resp_json
    assert "orderStatuses" in resp_json
    for elem in resp_json["getOrders"]:
        assert elem["totalAmount"] is not None
        assert "orderStatusId" in elem
        assert "shipping_address" in elem
        assert "billing_address" in elem


def test_get_list_of_delivery_services():
    r = get_list_of_delivery_services()

    list_of_del_serv = ["USPS", "Aramex", "Deutsche Post DHL", "Ukrposhta", "UPS", "Ukraine EMS", "DHL Parcel", "Fedex"]

    assert 200 == r.status_code
    resp_json = r.json()
    for elem in resp_json["deliveryServiceObject"]:
        assert elem["name"] in list_of_del_serv



