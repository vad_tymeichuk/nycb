import requests
from helpers.api.common_helpers import api_url, headers, cookies


def get_tax_rates():
    url = api_url + "/api/tax-rates/all?limit=10&page=1"

    return requests.get(url=url, headers=headers, cookies=cookies)


def test_get_rates():
    r = get_tax_rates()

    assert 200 == r.status_code
    resp_json = r.json()
    assert resp_json["rows"][0]["country_code"] is not None
    assert resp_json["rows"][0]["state_code"] is not None
    assert resp_json["rows"][0]["rate"] is not None
    assert resp_json["rows"][0]["tax_name"] is not None
    # assert resp_json["rows"][0]["country_codeId"] is not None
    # assert resp_json["rows"][0]["state_codeId"] is not None