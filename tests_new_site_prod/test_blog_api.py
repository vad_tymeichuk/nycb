import pytest
import requests

from helpers.api.blog_helpers import get_popular_posts, get_article_by_id
from helpers.api.common_helpers import api_url, headers, cookies


def test_get_popular_posts():
    pop_post_elem = ["id", "title", "img_path", "slug", "comments", "date"]
    r = get_popular_posts()
    resp_json = r.json()

    for elem in resp_json["BlogPopular"]:
        for elem1 in pop_post_elem:
            assert elem1 in elem


def test_get_article_by_id():
    article_id = 19
    article_title = "Backyard Wedding Dresses Ideas"
    r = get_article_by_id(article_id)
    resp_json = r.json()
    aricle_elems = ["id", "title_en", "title_fr", "title_es", "text_en", "text_fr", "text_es", "short_description_en",
                    "short_description_es", "short_description_fr", "author", "img_path", "slug", "date", "views",
                    "show", "migrate", "createdAt", "updatedAt", "deletedAt", "user"]

    assert article_id == resp_json["article"]["id"]
    assert article_title == resp_json["article"]["title_en"]
    for elem in aricle_elems:
        assert elem in resp_json["article"]


@pytest.mark.skip(reason="Unclear why test fails - work in Postman")
def test_get_list_article_blog_admin():
    url = api_url + "/api/admin/blog/get-list"
    param_set = {'limit': '2', 'page': '8'}
    r = requests.get(url=url, headers=headers, params=param_set, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert "articles" in resp_json
    assert "count" in resp_json
    assert "page" in resp_json
    assert "pageCount" in resp_json


def test_get_blog_to_client():
    url = api_url + "/api/blog/get-articles"
    param_set = {'page': '1', 'limit': '1'}
    r = requests.get(url=url, headers=headers, params=param_set, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert "BlogAllOut" in resp_json
    assert "nextPage" in resp_json["BlogAllOut"]


def test_get_all_post_to_client():
    url = api_url + "/api/blog"
    r = requests.get(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert "BlogAllOut" in resp_json
    assert "comments" in resp_json["BlogAllOut"][0]


def test_get_blog_to_client_by_slug():
    slug = "backyard-wedding-dresses-ideas"
    url = api_url + "/api/blog/" + slug
    r = requests.get(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert "BlogPopular" in resp_json
    assert "comments" in resp_json["BlogPopular"][0]


