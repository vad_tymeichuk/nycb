import json
import requests
from helpers.utils import settings

root_url = settings["url"]
headers = {"Content-type": "application/json",
           "User-Agent": "PostmanRuntime/7.26.8"}


def test_verify_blog():
    url = root_url + "/ny-city-bride-blog/"
    """Verify blog page opens"""
    r = requests.get(url=url, headers=headers)
    assert r.status_code == 200
    resp_text = r.text
    assert "NY City Bride - Blog" in resp_text
    assert "POPULAR POSTS" in resp_text


def test_verify_reviews():
    url = root_url + "/reviews/"
    """Verify reviews page opens"""
    r = requests.get(url=url, headers=headers)
    assert r.status_code == 200
    resp_text = r.text
    assert "joyful experience" in resp_text
    assert "Reviews" in resp_text
    assert "Wedding dress" in resp_text


