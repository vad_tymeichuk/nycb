import json
import requests
from helpers.utils import settings

root_url = settings["url"]
headers = {"Content-type": "application/json",
           "User-Agent": "PostmanRuntime/7.26.8"}


def test_verify_search_1():
    """Verify Search multiple products match"""
    params = {"s": "Sien",
              "post_type": "product"}
    r = requests.get(url=root_url, headers=headers, params=params)
    assert r.status_code == 200
    resp_text = r.text
    assert "sienna" in resp_text
    assert "siena" in resp_text


def test_verify_search_2():
    """Verify Search one product match"""
    params = {"s": "M-0104",
              "post_type": "product"}
    r = requests.get(url=root_url, headers=headers, params=params)
    assert r.status_code == 200
    resp_text = r.text
    assert "M-0104" in resp_text
    assert "Production time: 6-7 weeks" in resp_text


def test_verify_filter_1():
    """Verify filter with category + 3 attributes"""
    params = {"swoof": "1",
              "pa_silhouette": "a-line",
              "pa_sleeve-style": "long-sleeves",
              "pa_neckline": "v-neck",
              "really_curr_tax": "2645-product_cat"
              }
    r = requests.get(url=root_url, headers=headers, params=params)
    assert r.status_code == 200
    resp_text = r.text
    assert "Wedding dress 5205" or "Wedding dress S-574-ILARIA" in resp_text


def test_verify_filter_2():
    """Verify filter with category + attribute + tag"""
    params = {"swoof": "1",
              "product_tag": "2021-wedding-dresses",
              "pa_neckline": "high-neck",
              "really_curr_tax": "2317-product_cat"
              }
    r = requests.get(url=root_url, headers=headers, params=params)
    assert r.status_code == 200
    resp_text = r.text
    assert "Wedding dress Anna" in resp_text
