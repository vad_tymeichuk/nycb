# FROM python:3.6.9-slim-stretch
FROM joyzoursky/python-chromedriver

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD ["./scripts/run_all_tests.sh"]
