Feature: Verify "pay deposit" and "custom made" options
  Verify that "pay deposit" and "custom made" option have been applied

  Background: Shared steps for deposit and custom made options
    Given I am on "wedding-dresses" page
    Given I click on random product in the list

  Scenario: Apply "custom_made" option
#    Given I open main menu
#    When I click on the "All wedding dresses" link
#    When I click on first product in the list
    When I click on the "Custom Size +" checkbox
    When I click on the "Add to cart" button
    When I click on the "View cart" button
    Then I verify that "CUSTOM SIZE +" text is against product name in the cart

  Scenario: Apply "pay deposit" option
#    Given I open main menu
#    When I click on the "All wedding dresses" link
#    When I click on first product in the list
    When I click on the "Pay Deposit" button
    When I click on the "Add to cart" button
    When I click on the "View cart" button
    Then I make sure that "payable in total" text is against product name in the cart