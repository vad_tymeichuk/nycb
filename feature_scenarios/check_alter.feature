Feature: Verify that login and alteration request form is working properly
  Verify that user can send a request for alteration services recommendations

#  Scenario: Verify login
#    Given I am on the main page and click on My Account
#    When I enter "nycb-qa-test@riseserv.com", password and click Submit button
#    Then I am logged in my account with "nycb-qa-test@riseserv.com"

  Scenario: Verify alteration form
    Given I am logged in with "nycb-qa-test@riseserv.com"
    When I go to "alteration-recommendations" page
    And I enter "USA", "CA", "San Francisco", "94016", "Riseservise Developer", "nycb-qa-test@riseserv.com" and click Submit button
    Then I receive message "Thank you. Your message has been sent successfully."
