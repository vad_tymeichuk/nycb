Feature: Verify login is working properly
  Verify that user can log into their account

  Scenario: Verify login
#    Given I am on the main page closing the banner
    When I go to My Account, enter "nycb-qa-test@riseserv.com", password and click Submit button
    Then I am logged in my account with "nycb-qa-test@riseserv.com"
