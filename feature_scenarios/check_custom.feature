Feature: Verify "custom_made" option
  Verify that "custom_made" option has been applied

  Scenario: Apply "custom_made" option
    Given I open main menu
    When I click on the "All wedding dresses" link
    When I click on first product in the list
    When I click on the "Custom made" checkbox
    When I click on the "Add to cart" button
    When I click on the "View cart" button
    Then "Custom Made +" text is against product name in the cart
