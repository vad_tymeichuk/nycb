Feature: Verify the main menu links
  Verify that all the links in the main menu redirects to proper pages

  Background: Shared step for every scenario
    Given I open main menu

  Scenario: Verify "All wedding dresses" link
    When I click on the "All wedding dresses" link
    Then I verify page header "Wedding Dresses"

  Scenario: Verify "New Arrivals" link
    When I click on the "New Arrivals" link
    Then I verify page header "New Arrivals"

  Scenario: Verify "Plus Size Wedding Dresses" link
    When I click on the "Plus Size Wedding Dresses" link
    Then I verify page header "Plus Size Wedding Dresses"

  Scenario: Verify "Dresses under $1000" link
    When I click on the "Dresses under $1000" link
    Then I verify price range "250" to "1060" and page header "Wedding Dresses"

  Scenario: Verify "Wedding Dresses on Sale" link
    When I click on the "Sale" link
    Then I verify page header "Wedding Dresses on Sale"

  Scenario: Verify "Size Guide" link
    When I click on the "Size Guide" link
    Then I verify page header "Measurement Guide"

  Scenario: Verify "Alteration Shops" link
    When I click on the "Alteration Shops" link
    Then I verify page header "Alteration recommendations"

  Scenario: Verify "Our Brides" link
    When I click on the "Our Brides" link
    Then I verify page header "Our Brides"

  Scenario: Verify "Ready to Ship" page"
    When I click on the "Ready to Ship" link
    Then I verify page header "Wedding Dresses" and "in-stock-clearance" filter label