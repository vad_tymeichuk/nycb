import datetime
import email, smtplib, ssl
import os
import json
import requests
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


absolute_path_to_current_dir = os.path.dirname(os.path.abspath(__file__))


def load_settings():
    global settings
    settings_file = '../settings_production.json'
    print(os.environ.get("SITE_SETTINGS"))
    if os.environ.get("SITE_SETTINGS") is not None:
        if "staging" in os.environ.get("SITE_SETTINGS"):
            settings_file = '../settings_staging.json'
        elif "production" in os.environ.get("SITE_SETTINGS"):
            settings_file = '../settings_production.json'
        else:
            settings_file = '../settings_old_site.json'
    with open(os.path.join(absolute_path_to_current_dir, settings_file)) as f:
        return json.load(f)


settings = load_settings()

subject = "Test results for {} ".format(settings['url'])
body = "Your {} test results status are: \n {} \n".format(os.environ.get("ENVIRONMENT"), os.environ.get("WHICH_TEST"))
sender_email = settings["email-settings"]["sender"]
receiver_email = settings["email-settings"]["recipients"]
password = os.environ["MAIL_PASS_TEMP"]


def slack_sender(status, failure):
    slack_hook_url = settings["slack-settings"]["webhook"]
    data = {"text": status} if not failure else {"attachments": [{
                "pretext": "One or more of your tests failed!",
                "title": "Test results:",
                "text": status,
                "color": "#FF0000"
                }]
            }
    headers = {"Content-type": "application/json",
               "Host": "hooks.slack.com"}
    r = requests.post(url=slack_hook_url, data=json.dumps(data), headers=headers)
    print("DEBUG ===>> Status code: " + str(r.status_code))
    if r.status_code != 400:
        print(data)
        print("SLACK MESSAGE SENT SUCCESSFULLY @ " + str(datetime.datetime.now()))


def searchfiles(extension='.png', folder=os.path.join(absolute_path_to_current_dir, '../screenshot')):
    files_found = []
    for r, d, f in os.walk(folder):
        for file in f:
            if file.endswith(extension):
                files_found.append(f'{r + "/" + file}')

    return files_found


def mail_sender(test_results_message, add_attachments):
    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email  # Recommended for mass emails

    # Add body to email
    message.attach(MIMEText(body + test_results_message, "plain"))

    # Add attachments
    if add_attachments:
        files = searchfiles()
        # Find report.html in same directory as script
        report_filename = "report.html" if os.environ.get("REPORT_OUTPUT") is None else os.environ.get("REPORT_OUTPUT")
        files.append(report_filename)

        for filename in files:
            # Open PDF file in binary mode
            with open(filename, "rb") as attachment:
                # Add file as application/octet-stream
                # Email client can usually download this automatically as attachment
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.read())

            # Encode file in ASCII characters to send by email
            encoders.encode_base64(part)

            # Add header as key/value pair to attachment part
            part.add_header(
                "Content-Disposition",
                "attachment; filename={}".format(filename),
            )

            # Add attachment to message and convert message to string
            message.attach(part)

    text = message.as_string()

    # Log in to server using secure context and send email
    server = smtplib.SMTP_SSL(settings["email-settings"]["mail-server"], int(settings["email-settings"]["port"]))
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email.split(','), text)
    print("EMAIL SENT SUCCESSFULLY")


def send_notifications():
    if (os.environ.get("ENVIRONMENT") != "PRODUCTION") and (os.environ.get("ENVIRONMENT") != "STAGING"):
        return

    try:
        passed_count = 0
        failed_count = 0
        skipped_count = 0
        rerun_count = 0
        with open('.report.json') as f:
            json_test_results = json.load(f)
            print(json_test_results["summary"])
            if "passed" in json_test_results["summary"]:
                passed_count = json_test_results["summary"]["passed"]
            if "failed" in json_test_results["summary"]:
                failed_count = json_test_results["summary"]["failed"]
            if "skipped" in json_test_results["summary"]:
                skipped_count = json_test_results["summary"]["skipped"]
            if "rerun" in json_test_results["summary"]:
                rerun_count = json_test_results["summary"]["rerun"]
    except FileNotFoundError as e:
        err_msg = "File .report.json was not found: \n" + str(e.__cause__)
        slack_sender(err_msg, True)
        mail_sender(err_msg, False)
        return

    passed_failed_line = "{} PASSED, {} RERUN, {} SKIPPED, {} FAILED\n"\
        .format(str(passed_count), str(rerun_count), str(skipped_count), str(failed_count))
    environment_line = "Tests: {}, Env: {}, URL: {}\n"\
        .format(os.environ.get("WHICH_TEST"),
                os.environ.get("ENVIRONMENT"),
                settings['url'])

    email_send_on_success = settings["notifications"]["email"]["on-success"]
    email_send_on_failure = settings["notifications"]["email"]["on-failure"]
    if (email_send_on_success and failed_count == 0) or (email_send_on_failure and failed_count > 0):
        try:
            mail_sender(passed_failed_line + environment_line, True)
            print("Email was sent to: " + settings["email-settings"]["recipients"])
        except Exception as err:
            print("=> email error: {0}".format(err))
    else:
        print("""Email was Not sent, because failed tests={} and passed={}, 
        email notifications settings on-failure: {}, on-success: {}"""
              .format(failed_count, passed_count, email_send_on_failure, email_send_on_success))

    slack_send_on_success = settings["notifications"]["slack"]["on-success"]
    slack_send_on_failure = settings["notifications"]["slack"]["on-failure"]
    if (slack_send_on_success and failed_count == 0) or (slack_send_on_failure and failed_count > 0):
        slack_sender(passed_failed_line + environment_line, failed_count != 0)
        print("Slack message was sent!")
    else:
        print("""Slack message was Not sent, because failed tests={} and passed={}, 
        Slack notifications settings on-failure: {}, on-success: {}"""
              .format(failed_count, passed_count, slack_send_on_failure, slack_send_on_success))


# notification sending is triggered here
send_notifications()
