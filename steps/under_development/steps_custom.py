from pytest_bdd import given, when, then
from helpers.funcs import *
from steps.steps_common import *


@when('I click on the "Custom made" checkbox')
def check_custom_made(driver, teardown):
    feb_id_click(driver, 'pewc_group_200033_200034')


@when('I click on the "Add to cart" button')
def add_to_cart(driver, teardown):
    feb_xp_click(driver, "//div/button[@type='submit']")


@when('I click on the "View cart" button')
def click_view_cart(driver, teardown):
    feb_xp_click(driver, "//a[@class='button wc-forward']")


@then('"Custom Made +" text is against product name in the cart')
def verify_custom_made(driver, teardown):
    verify_custom(driver, "//dt[@class='variation-CustomSize']")

