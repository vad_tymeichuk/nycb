from pytest_bdd import given, when, then
from helpers.funcs import *
from steps.steps_common import *


@when('I click on the "Pay deposit" button')
def select_deposit(driver, teardown):
    feb_xp_click(driver, "//label[@for='wc-option-pay-deposit']")


@when('I click on the "Add to cart" button')
def add_to_cart(driver, teardown):
    feb_xp_click(driver, "//div/button[@type='submit']")


@when('I click on the "View cart" button')
def click_view_cart(driver, teardown):
    feb_xp_click(driver, "//a[@class='button wc-forward']")


@then('"payable in total" text is against product name in the cart')
def verify_deposit_(driver, teardown):
    verify_deposit(driver, "//td[@class='product-subtotal']/small")
