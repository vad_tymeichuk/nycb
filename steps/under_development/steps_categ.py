from helpers.funcs import *
from helpers.utils import filename
from helpers.utils import report_name


def verify_category(driver):
    # Close banner
    close_banner(driver)

    # Go to "Wedding dresses" page (Home -> Wedding Dresses -> All wedding dresses)
    feb_cn_click(driver, 'mega-menu-link')
    feb_lt_click(driver, 'All wedding dresses')

    largest = 0
    full_list_of_dresses = {}
    pages_total = find_all_xp(driver, "//a[@class='next page-numbers']/preceding::a[@class='page-numbers']")

    for page in pages_total:
        if int(page.text) > largest:
            largest = int(page.text)

    # for page in range (2, largest+1):
    for page in range(2, 6):  # comment this and uncomment the line above to check all pages in Wedding Dresses
        list_of_dresses = find_all_dresses(driver, "woocommerce-loop-product__title")
        for dress in list_of_dresses:
            if "wedding" not in dress.text.lower():
                full_list_of_dresses[dress.text] = find_lt(driver, dress.text).get_attribute('href')
        feb_xp_click(driver, "//a[@class='page-numbers'][text()='{}']".format(page))

    report = open(filename(), "w")

    for key, value in full_list_of_dresses.items():
        report.write(key + "\n" + value + "\n" + "\n")
        # print(key)

    report.close()

    if len(full_list_of_dresses) == 0:
        assert True
    else:
        assert False, "check the file reports/" + report_name + ", there are " + str(len(full_list_of_dresses)) \
                      + " wrong dresses"

