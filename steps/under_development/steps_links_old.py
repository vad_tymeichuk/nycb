from helpers.funcs import *


def click_all_menu_links(driver):
    # Close banner
    close_banner(driver)

    # Go to "Wedding dresses" page (Home -> Wedding Dresses -> All wedding dresses)
    feb_cn_click(driver, 'mega-menu-link')
    feb_lt_click(driver, 'All wedding dresses')
    # Get page's heading
    wed_dress = get_page_heading(driver)

    # Go to "New Arrivals" page (Home -> All wedding dresses -> New Arrivals)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "New Arrivals")
    # Get page's heading
    new_arriv = get_page_heading(driver)

    # Go to "Plus Size Wedding Dresses" page (Home -> All wedding dresses -> Plus Size Wedding Dresses)
    feb_cn_click(driver, 'mega-menu-link')
    feb_lt_click(driver, 'Plus Size Wedding Dresses')
    # Verify you are on the correct page
    plus_size = verify_plus_size(driver, "Plus Size")

    # Go to "Dresses under $400" page (Home -> All wedding dresses -> Dresses under $400)
    feb_cn_click(driver, 'mega-menu-link')
    feb_lt_click(driver, 'Dresses under $1000')
    # Verify you are on the correct page
    under_1000 = verify_dress_price_range(driver, '250', '1060', "Wedding Dresses")

    # Go to "Wedding Dresses on Sale" page (Home -> All wedding dresses -> Sale)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "Sale")
    # Get page's heading
    dress_on_sale = get_page_heading(driver)

    # Go to "Measurement Guide" page (Home -> All wedding dresses -> Size Guide)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "Size Guide")
    # Get page's heading
    meas_guide = get_page_heading(driver)

    # Go to "Alteration recommendations" page (Home -> All wedding dresses -> Alteration Shops)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "Alteration Shops")
    # Get page's heading
    alter_recom = get_page_heading(driver)

    # Go to "Our Brides" page (Home -> All wedding dresses -> Our Brides)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "Our Brides")
    # Get page's heading
    brides = get_page_heading(driver)

    # Go to "Ready To Ship" ("In Stock") page (Home -> All wedding dresses -> Ready to ship)
    feb_cn_click(driver, "mega-menu-link")
    feb_lt_click(driver, "Ready to Ship")
    wait_for(4)
    # Verify you are on the correct page
    in_stock = verify_in_stock(driver, "In Stock", "Wedding Dresses")

    assert wed_dress.lower() == "Wedding Dresses".lower()
    assert new_arriv.lower() == "NEW ARRIVALS".lower()
    assert plus_size
    assert under_1000
    assert dress_on_sale.lower() == "WEDDING DRESSES ON SALE".lower()
    assert meas_guide.lower() == "MEASUREMENT GUIDE".lower()
    assert alter_recom.lower() == "ALTERATION RECOMMENDATIONS".lower()
    assert brides.lower() == "Our Brides".lower()
    assert in_stock

