from pytest_bdd import given, when, then, parsers
from helpers.funcs import *


@given(parsers.parse('I am logged in with "{email}"'))
def log_in(driver_module, email, teardown_module):
    login(driver_module, email)


@when(parsers.parse('I go to "{link}" page'))
def go_alter_shops(driver_module, link, teardown_module):
    go_to_page(driver_module, link)


@when(parsers.parse('I enter "{country}", "{state}", "{city}", "{zip}", "{name}", "{email}" and click Submit button'))
def fill_form(driver_module, country, state, city, zip, name, email, teardown_module):
    fill_alter_form(driver_module, country, state, city, zip, name, email)


@then(parsers.parse('I receive message "{message}"'))
def verify(driver_module, message, teardown_module):
    verify_alter_form(driver_module, message)
    # logout(driver)
