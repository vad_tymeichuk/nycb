from pytest_bdd import given, when, then, parsers
from helpers.funcs import *


@given('I open main menu')
def open_mega_menu(driver, teardown):
    open_main_menu(driver)


@when(parsers.parse('I click on the "{link}" link'))
def click_on_link(driver, link, teardown):
    click_link(driver, link)


@given('I click on random product in the list')
def click_on_random_product(driver, teardown):
    click_first_product(driver)
