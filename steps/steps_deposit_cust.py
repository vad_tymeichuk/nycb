from pytest_bdd import given, when, then
from helpers.funcs import *
from steps.steps_common import *


@given(parsers.parse('I am on "{link}" page'))
def go_wedding_dresses(driver, link, teardown):
    go_to_page(driver, link)


@when(parsers.parse('I click on the "{custom}" checkbox'))
def check_custom_made(driver, custom, teardown):
    click_custom_made(driver, custom)


@then(parsers.parse('I verify that "{text}" text is against product name in the cart'))
def verify_custom_made(driver, text, teardown):
    verify_custom(driver, text)


@when(parsers.parse('I click on the "{deposit}" button'))
def select_deposit(driver, deposit, teardown):
    click_pay_deposit(driver, deposit)


@when('I click on the "Add to cart" button')
def add_to_cart(driver, teardown):
    click_add_to_cart(driver)


@when('I click on the "View cart" button')
def click_view_cart_(driver, teardown):
    click_view_cart(driver)


@then(parsers.parse('I make sure that "{text}" text is against product name in the cart'))
def verify_deposit_(driver, text, teardown):
    verify_deposit(driver, text)


