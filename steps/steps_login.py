from pytest_bdd import given, when, then, parsers
from helpers.funcs import *


# this step is not used
@given('I am on the main page closing the banner')
def go_my_account(driver_module, teardown_module):
    pass


@when(parsers.parse('I go to My Account, enter "{email}", password and click Submit button'))
def log_in(driver_module, email, teardown_module):
    login(driver_module, email)


@then(parsers.parse('I am logged in my account with "{email}"'))
def login_verify(driver_module, email, teardown):
    verify_login(driver_module, email)
