from pytest_bdd import given, when, then, parsers
from helpers.funcs import *
from steps.steps_common import *


@then(parsers.parse('I verify page header "{header}"'))
def verify_page_heading(driver, header, teardown):
    assert get_page_heading(driver).lower() == header.lower()


@then(parsers.parse('I verify price range "{min_price}" to "{max_price}" and page header "{header}"'))
def verify_page_heading_range(driver, min_price, max_price, header, teardown):
    verify_dress_price_range(driver, min_price, max_price, header)


@then(parsers.parse('I verify page header "{header}" and "{filter_label}" filter label'))
def verify_page_heading_range(driver, header, filter_label, teardown):
    verify_in_stock(driver, filter_label, header)
