#!/bin/sh
export REPORT_OUTPUT="report.html"

if [ -z "$WHICH_TEST" ]
then
  export WHICH_TEST="./tests"
fi

# Run tests
pytest $WHICH_TEST --json-report --html=${REPORT_OUTPUT} --self-contained-html --reruns 3 --reruns-delay 10
# Send results
sleep 10
python ./notification_workers/send_notifications.py

