# Repository for E2E integration tests

1.  Python version suggested: `3.6.9` . You can check existing installed version: `python3 --version`
    In order to install specific of Python, you can use [pyenv](https://github.com/pyenv/pyenv) tool
    [virtualenv](https://virtualenv.pypa.io/en/latest) is preferred to isolate project dependencies.
    You can follow these steps to install dependencies:

        virtualenv --python python3 env
        source env/bin/activate
        pip install -r requirements.txt

3. Env variables 
        
        SITE_SETTINGS - defines which settings fiel will be picked up. 
        By deafult, settings_production.json will be picked up. 
        Pssible values: "old_site", "staging", "production"
        
        ENVIRONMENT - defines wher ethe tests are running. 
        Values: "PRODUCTION", "STAGING", "<any value>"
        
        WHICH_TEST - variable that defines which tests will be executed. 
        Can take "-k test_....py" or "./tests/test_....py"

4. Configure your tests in settings.json
       
       settings.json

5.  Now you can run all tests with [pytest](https://docs.pytest.org/en/latest/) tool:

        pytest -k test_deposit.py
        
        Include parameters to generate test result report
        pytest -k test_deposit.py --html=report.html --self-contained-html

6.  Run individual component with rerun option on failure: 
            
        pytest -k test.py --reruns 4 --reruns-delay 1 --disable-pytest-warnings
        
7. Docker container command

        #Clean python cache before generating a new image
        find . | grep -E "(__pycache__|__pytest_cache__|.pytest_cache|\.pyc|\.pyo$)" | xargs rm -rf
             
        docker build . -t registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test -f Dockerfile
        docker container run registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test
        docker push registry.digitalocean.com/nycb-riseserv-eng-qaa/email-sender-test

8. Send mail with the latest test results
        
        Make sure to set MAIL_PASS variable in .bash_profile as export MAIL_PASS="your @riseserv.com email password"
        Run: pytest test_e.py

9. Kubernetes commands  
        
        # create new cronjob 
        kubectl apply -f qaa-deploy.yaml
        
        # create new job
        kubectl apply -f qaa-run-job.yaml
        
        # delete existing job (previously created job with the corresponding name in qaa-run-job.yaml will be deleted)
        kubectl delete -f qaa-run-job.yaml
        
        # save changes to secrets
        kubectl apply -f secrets.yaml
        
        # get cron jobs
        kubectl get cronjob
        
        # get cron job
        kubectl get cronjob <cronjob_name>
        
        # get jobs
        kubectl get job
        
        # get job
        kubectl get cronjob <job_name>
        
        # describe objects
        kubectl describe cronjob qaa-run
        kubectl delete cronjob <job_name>
        kubectl describe jobs/<job_name>
        
        # view job logs
        kubectl get pods -A
        kubectl logs <pod_name>
        kubectl logs $(echo $(kubectl get pods -A | grep 'qaa-run-job-manually-0.0.0') | cut -c 9-40)
        

10. Decode your password: 
        
        echo  'password' | base64
