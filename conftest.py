import json

import pytest
from selenium import webdriver
from helpers.funcs import *

from helpers.utils import settings


@pytest.fixture(scope="session")
def driver():
    options = webdriver.ChromeOptions()
    if settings['mode'] == "headless":
        options.headless = True
        options.add_argument('window-size=1920x1480')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-browser-side-navigation")
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        browser = webdriver.Chrome(options=options)
        browser.maximize_window()
    else:
        browser = webdriver.Chrome()
        browser.maximize_window()

    browser.get(settings['url'])
    # close_banner(browser)
    # browser.refresh()
    return browser


@pytest.fixture(scope="session")
def teardown(driver):
    yield driver
    driver.close()


@pytest.fixture(scope="module")
def teardown_module(driver_module):
    yield driver_module
    driver_module.close()


@pytest.fixture(scope="module")
def driver_module():
    options = webdriver.ChromeOptions()
    if settings['mode'] == "headless":
        options.headless = True
        options.add_argument('window-size=1920x1480')
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-browser-side-navigation")
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        browser = webdriver.Chrome(options=options)
        browser.maximize_window()
    else:
        browser = webdriver.Chrome()
        browser.maximize_window()

    browser.get(settings['url'])
    # close_banner(browser)
    # browser.refresh()
    return browser


#
# @pytest.hookimpl(tryfirst=True, hookwrapper=True)
# def pytest_runtest_makereport(item, call):
#     outcome = yield
#     rep = outcome.get_result()
#     if call.when == "call":
#         f = open("status.txt", "a")
#         if rep.outcome == 'failed':
#             f.write("FAILED ")
#         elif rep.outcome == 'skipped':
#             f.write("SKIPPED ")
#         elif rep.outcome == 'passed':
#             f.write("PASSED ")
#
#         f.close()

