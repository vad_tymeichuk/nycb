import json
import os, time

# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.common.exceptions import TimeoutException
# from selenium.webdriver.support.ui import Select
from datetime import datetime


def plus_one_day(date):
    month = int(date[0:2])
    day = int(date[3:5])
    year = int(date[6:])
    if month < 12:
        if day < 28:
            new_date = "{}/{}/{}".format(month, day + 1, year)
        else:
            new_date = "{}/{}/{}".format(month + 1, 1, year)
    else:
        if day < 28:
            new_date = "{}/{}/{}".format(month, day + 1, year)
        else:
            new_date = "{}/{}/{}".format(1, 1, year + 1)

    return new_date


def get_time():
    return datetime.now().strftime("%Y-%m-%d_%H-%M-%S")


def get_screenshot_name():
    return 'test_run_at-{}.png'.format(get_time())


now = get_time()
date = datetime.now().strftime("%d-%m-%Y")
date_ = datetime.now().strftime("%m/%d/%Y")
time_ = datetime.now().strftime("%H:%M")
date_time = datetime.now().strftime("%m/%d/%Y-%H:%M:%S")

# ../../screenshot from current file dir
path = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))
screen_dir = os.path.join(path, "screenshot", str(date))
screen_dir1 = os.path.join(path, "reports")

screenshot_name = 'test_run_at-{}.png'.format(get_time())
report_name = 'test_run_at' + '-' + now + ".txt"

absolute_path_to_current_dir = os.path.dirname(os.path.abspath(__file__))


# def load_settings(settings_file='../settings_production.json'):
#     global settings
#     with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), settings_file)) as f:
#         return json.load(f)

def load_settings():
    settings_file = '../settings_production.json'
    print(os.environ.get("SITE_SETTINGS"))
    if os.environ.get("SITE_SETTINGS") is not None:
        if "staging" in os.environ.get("SITE_SETTINGS"):
            settings_file = '../settings_staging.json'
        elif "production" in os.environ.get("SITE_SETTINGS"):
            settings_file = '../settings_production.json'
        else:
            settings_file = '../settings_old_site.json'
    with open(os.path.join(absolute_path_to_current_dir, settings_file)) as f:
        return json.load(f)


settings = load_settings()
# settings_old_site = load_settings('../settings_old_site.json')


def get_wait(driver, wait=10):
    return WebDriverWait(driver, wait)


def screen_path():
    global screen_dir
    if not os.path.exists(screen_dir):
        os.makedirs(screen_dir)
    return screen_dir


def save_screenshot(driver):
    screenshot_path = os.path.join(screen_path(), get_screenshot_name())
    driver.get_screenshot_as_file(screenshot_path)
    return screenshot_path


def filename():
    global screen_dir1
    if not os.path.exists(screen_dir1):
        os.makedirs(screen_dir1)
    return os.path.join(screen_dir1, report_name)