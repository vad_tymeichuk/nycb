import json
import os
import random

import requests
from helpers.api.common_helpers import api_url, headers, cookies


def upload_image(api_path):
    url = api_url + api_path
    abs_path = os.path.dirname(os.path.abspath(__file__))
    filename = 'wed-dress-clementine-{}.jpeg'.format(random.choice([1, 2, 3]))
    path_to_img = os.path.abspath(os.path.join(abs_path, '../files/{}'.format(filename)))
    print(path_to_img)
    img = open(path_to_img, 'rb')
    # do not use headers since they have {"Content-type": "application/json"}, but "image/jpeg" needed
    files = {"image": (filename, img, "image/jpeg")}
    r = requests.post(url=url, files=files, cookies=cookies)

    img.close()

    return r


def upload_several_images(api_path):
    url = api_url + api_path
    abs_path = os.path.dirname(os.path.abspath(__file__))
    filename1 = 'wed-dress-clementine-1.jpeg'
    filename2 = 'wed-dress-clementine-2.jpeg'
    path_to_img1 = os.path.abspath(os.path.join(abs_path, '../files/{}'.format(filename1)))
    path_to_img2 = os.path.abspath(os.path.join(abs_path, '../files/{}'.format(filename2)))
    print(path_to_img1)
    img1 = open(path_to_img1, 'rb')
    img2 = open(path_to_img2, 'rb')
    # do not use headers since they have {"Content-type": "application/json"}, but "image/jpeg" needed
    files = [("image", (filename1, img1, "image/jpeg")), ("image", (filename2, img2, "image/jpeg"))]
    r = requests.post(url=url, files=files, cookies=cookies)

    img1.close()
    img2.close()

    return r


def get_image_id(upload_resp_json):
    # r = upload_image()
    # resp_json = r.json()

    return upload_resp_json["images"][0]["id"]


def get_image_ids(upload_resp_json):
    ids = []
    for elem in upload_resp_json["images"]:
        ids.append(elem["id"])

    return ids


def delete_image(id_):
    url = api_url + "/api/our-brides/delete-photo/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)