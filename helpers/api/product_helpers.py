import json
import random
import requests
from helpers.api.common_helpers import api_url, headers, cookies


def get_product_slug_by_id(product_id):
    url = api_url + "/api/admin/product/" + str(product_id)
    resp_json = requests.get(url=url, headers=headers, cookies=cookies).json()
    return resp_json["product"]["slug"]


def get_parent_categories_tree_admin():
    url = api_url + "/api/categories/category-tree-admin"
    return requests.get(url=url, headers=headers, cookies=cookies)


def get_parent_categories_tree():
    url = api_url + "/api/categories/category-tree"
    return requests.post(url=url, headers=headers, cookies=cookies)


def get_product_price(id_):
    url = api_url + "/api/admin/product/{}/variation".format(str(id_))

    resp_json = requests.get(url=url, headers=headers, cookies=cookies).json()

    return resp_json["getVariation"][0]["price"]
