import json
import os
import requests

from helpers.utils import settings, now, date_time, date_, time_, plus_one_day

api_url = settings["api-url"]
# api_url_staging = settings["api-url-staging"]
# api_url_production = settings["api-url-production"]
headers = {"Content-type": "application/json"}
test_api_root_user_email = "nycb20@riseservice.com.ua"
test_api_root_user_password = os.environ["TEST_API_MAIL_PASS"]
# test_api_user_email = "nycb-qa-test{}@riseserv.com".format(now)
test_api_user_email = "nycb-qa-test@riseserv.com.ua"
test_api_user_password = str(os.environ["TEST_API_MAIL_PASS"]).rstrip() + "A"


def login_get_cookies(email=test_api_root_user_email, passwd=test_api_root_user_password):
    url = api_url + "/api/auth/login"
    body = {
        "email": email,
        "password": passwd
    }
    print(email, passwd)

    r = requests.post(url=url, headers=headers, data=json.dumps(body))
    resp_json = r.json()
    if "successFalse" in resp_json:
        raise Exception("User login failed because username/password not correct. "
                        "Response: " + str(resp_json))
    elif resp_json["login"] is True and r.status_code == 200 or resp_json["login"] is False and r.status_code == 403:
        return {"auth": r.cookies['auth']}
    else:
        raise Exception("User login failed, response: " + str(resp_json))


cookies = login_get_cookies()


