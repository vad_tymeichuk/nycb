from helpers.api.common_helpers import *


def register():
    url = api_url + "/api/admin/users/register"
    body = {
        "img": "",
        "email": test_api_user_email,
        "username": "testuser" + now,
        "password": test_api_user_password,
        "f_name": "Riseservise",
        "l_name": "Developer",
        "role_id": 1
    }
    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def login():
    url = api_url + "/api/auth/login"
    body = {
        "email": test_api_user_email,
        "password": test_api_user_password
    }
    r = requests.post(url=url, headers=headers, data=json.dumps(body))
    resp_json = r.json()
    if "successFalse" in resp_json:
        # reg = register()
        # print("REGISTRATION =>>>>>>>>" + str(reg.json()))
        # return requests.post(url=url, headers=headers, data=json.dumps(body))
        print(r.json())
        assert False
    else:
        print("Login successful!")
        return r
    #     print("=>>>>>>>> Another login attempt")
    #     return requests.post(url=url, headers=headers, data=json.dumps(body))

