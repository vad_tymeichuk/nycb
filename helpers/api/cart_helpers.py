import json
import random
import requests
from helpers.api.common_helpers import api_url, headers, cookies

product_name = "josie"
pay_type = ["full", "deposit"]
type_ = ["product", "swatch"]
cart_public_id = "6363ecd7-458c-4ed5-a90e-131d7057980b"
custom_made_options = [{
            "size": "12.5",
            "bust": "90",
            "waist": "60",
            "hips": "90",
            "sleeve_length": "19",
            "height": "175",
            "hollow_to_floor": "none",
            "armpit_to_floor": "150",
            "bicep": "20",
            "wrist": "11",
            "custom_size_notes": "Please make the best dress ever!"
        }, None]
custom_made = random.choice(custom_made_options)


def get_prod_id_by_name(prod_name):
    url = api_url + "/api/catalog/search"

    search_keyword = prod_name
    params = {"s": search_keyword}
    r = requests.get(url=url, params=params, headers=headers)
    resp_json = r.json()

    return resp_json["rows"][0]["id_product"]


product_id = get_prod_id_by_name(product_name)


def add_to_cart():
    url = api_url + "/api/cart/add"
    body = {"item": {
        "quantity": 1,
        "productsId": product_id,
        "pay_type": pay_type[0],
        "type": type_[0],
        "custom_made": custom_made,
        "attribute_variation": [
            {"attributeId": 1, "attributeTermId": 134},
            {"attributeId": 3, "attributeTermId": 10},
            {"attributeId": 13, "attributeTermId": 49}
        ],
        "cartPublicId": cart_public_id
        }
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def add_to_cart_2(prod_id):
    color = 134
    if prod_id == 164:
        color = 2
    url = api_url + "/api/cart/add"
    body = {"item": {
        "quantity": 1,
        "productsId": prod_id,
        "pay_type": pay_type[0],
        "type": type_[0],
        "custom_made": {
            "size": "s",
            "bust": "s",
            "waist": "s",
            "hips": "s",
            "sleeve_length": "s",
            "height": "s",
            "hollow_to_floor": "s",
            "armpit_to_floor": "s",
            "bicep": "s",
            "wrist": "s",
            "custom_size_notes": "s"
        },
        "attribute_variation": [
            {"attributeId": 1, "attributeTermId": color},
            {"attributeId": 3, "attributeTermId": 10},
            {"attributeId": 13, "attributeTermId": 49}
        ],
        "cartPublicId": cart_public_id
        }
    }

    return requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies)


def get_cart():
    url = api_url + "/api/cart"
    return requests.post(url=url, headers=headers, cookies=cookies)


def get_product_cart_id():
    r = get_cart()
    resp_json = r.json()
    return resp_json["items_cart"][0]["id"]


def delete_from_cart():
    product_cart_id = get_product_cart_id()
    url = api_url + "/api/cart/" + str(product_cart_id)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def delete_from_cart_by_cart_id(id_):
    # product_cart_id = get_product_cart_id()
    url = api_url + "/api/cart/" + str(id_)

    return requests.delete(url=url, headers=headers, cookies=cookies)


def clear_cart():
    resp_json = get_cart().json()
    prod_ids = []

    cart_items_quantity = len(resp_json["items_cart"])
    print("\n" + "Cart has {} elements now".format(cart_items_quantity))
    print("Cart elements id's are:")
    if cart_items_quantity > 0:
        for elem in resp_json["items_cart"]:
            prod_ids.append(elem["id"])
            print(elem["id"])

        print("Deleting all elements from cart ...")

        for elem1 in prod_ids:
            delete_from_cart_by_cart_id(elem1)

    print("Cart has {} elements now".format(len(get_cart().json()["items_cart"])))


def get_ship_cost_for_case_id(case_id):
    url = api_url + "/api/shipping-calculator/get-methods"
    body = {
        "hcShippingClassId": 2  # wedding dresses class
    }
    resp_json = requests.post(url=url, headers=headers, data=json.dumps(body), cookies=cookies).json()

    for elem in resp_json["ship_methods"]:
        for elem1 in elem["cases"]:
            if case_id == elem1["id"]:
                return elem1["one_item"]

    return "No shipping cases with  id = {} found".format(case_id)
