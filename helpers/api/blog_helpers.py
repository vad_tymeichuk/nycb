import json
import requests
from helpers.utils import settings, now
from helpers.api.common_helpers import *


def get_popular_posts():
    url = api_url + "/api/blog/popular-post"

    return requests.get(url=url, headers=headers, cookies=cookies)


def get_article_by_id(id_):
    url = api_url + "/api/admin/blog/get/" + str(id_)

    return requests.get(url=url, headers=headers, cookies=cookies)


def create_article():
    """Create Article"""
    url = "{}/api/admin/blog".format(api_url)
    suffix = str(now)
    data = {
        "title_en": "Auto-test-title-en" + suffix,
        "text_en": "Auto-test-title-en" + suffix,
        "slug": "Auto-test-slug" + suffix
    }
    r = requests.post(url=url, data=json.dumps(data), headers=headers, cookies=cookies)
    if r.status_code == 201:
        resp_json = r.json()
        return str(resp_json["id"])


def delete_article(_id):
    """Delete Article"""
    url = "{}/api/admin/blog/{}".format(api_url, _id)
    r = requests.delete(url=url, headers=headers, cookies=cookies)
    return r.status_code == 200


def get_article_slug_by_id(_id):
    url = "{}/api/admin/blog/get/{}".format(api_url, _id)
    r = requests.get(url=url, headers=headers, cookies=cookies)
    assert r.status_code == 200
    resp_json = r.json()
    assert "article" in resp_json
    assert "slug" in resp_json["article"]
    return resp_json["article"]["slug"]

