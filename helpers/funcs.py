import random

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from helpers.utils import save_screenshot, screenshot_name, get_wait
from datetime import datetime
from helpers.utils import settings
import time
import logging
import os

log = logging.getLogger(__name__)


def click_element_by(driver, by, locator):
    return find_element_by(driver, by, locator).click()


def find_element_by_if_error(driver):
    elem = get_wait(driver, 3).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(), 'Error 524')]")))
    return elem


"""will implement this function later"""
def find_element_by_(driver, by, locator, tries=2):
    try:
        elem = get_wait(driver).until(EC.presence_of_element_located((by, locator)))
        return elem
    except Exception as e:
        while tries > 0:
            try:
                error = find_element_by_if_error(driver)
                if "Error 524" in error.text:
                    print(error.text)
                    driver.refresh()
                    find_element_by(driver, by, locator, tries-1)
                    # save_screenshot(driver)
                    # assert False, 'check out screenshot ' + screenshot_name + '\n Error: ' + str(e)
            except TimeoutException:
                driver.refresh()
                find_element_by(driver, by, locator, tries-1)
            except:
                save_screenshot(driver)
                assert False, 'check out screenshot ' + screenshot_name + '\n Error: ' + str(e)


def find_element_by(driver, by, locator, tries=1):
    try:
        elem = get_wait(driver).until(EC.presence_of_element_located((by, locator)))
        return elem
    except Exception as e:
        if tries == 0:
            save_screenshot(driver)
            assert False, 'check out screenshot ' + screenshot_name + '\n Error: ' + str(e)
        driver.refresh()
        find_element_by(driver, by, locator, tries-1)


def find_all_elements_by(driver, by, locator):
    try:
        elems = get_wait(driver).until(EC.presence_of_all_elements_located((by, locator)))
        return elems
    except Exception as e:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name + '\n Error: ' + str(e)


def click_first_product(driver):
    click_element_by(driver, By.CLASS_NAME, 'woocommerce-loop-product__title')


def click_random_product(driver):
    products_on_page = find_all_elements_by(driver, By.CLASS_NAME, 'woocommerce-loop-product__title')
    random.choice(products_on_page).click()


def open_main_menu(driver):
    wait_for(1)
    actions = ActionChains(driver)
    main_menu = find_element_by_(driver, By.XPATH, "//a[text()='Wedding Dresses']/*[@class='mega-indicator']")
    actions.move_to_element(main_menu).perform()
    # click_element_by(driver, By.XPATH, "//a[text()='Wedding Dresses']/*[@class='mega-indicator']")
    # wait_for(1)


# def feb_lt_click(driver, link_t):
#     try:
#         find_element_by(driver, By.LINK_TEXT, link_t)
#     except Exception as e:
#         log.error("Screenshot: " + save_screenshot(driver))
#         raise Exception(e)


def click_link(driver, link_text):
    click_element_by(driver, By.LINK_TEXT, link_text)


def click_pay_deposit(driver, text):
    click_element_by(driver, By.XPATH, "//label[contains(text(), '{}')]".format(text))


def click_custom_made(driver, text):
    click_element_by(driver, By.XPATH, "//label[contains(text(), '{}')]".format(text))


def click_add_to_cart(driver):
    wait_for(1)
    click_element_by(driver, By.XPATH, "//div/button[@type='submit']")


def click_view_cart(driver):
    click_element_by(driver, By.XPATH, "//a[@class='button wc-forward']")


def verify_deposit(driver, depos_text):
    deposit = find_element_by(driver, By.XPATH, "//td[@class='product-subtotal']/small")
    try:
        assert depos_text.lower() in deposit.text.lower()
    except:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


def verify_custom(driver, custom_made):
    custom_made_found = find_element_by(driver, By.XPATH, "//dt[@class='variation-CustomSize']")
    print()
    try:
        assert custom_made.lower() in custom_made_found.text.lower()
    except:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


def print_elem_text(driver, locator):
    elem = find_element_by(driver, By.XPATH, locator)
    print(elem.text)


def verify_plus_size(driver, plus_size_text):
    elem = find_element_by(driver, By.XPATH, "//span[@class='woof_remove_ppi']")
    if elem.text.lower() == plus_size_text.lower():
        return True
    else:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


def verify_dress_price_range(driver, min_price, max_price, page_header):
    min_price_ = find_element_by(driver, By.XPATH, "//div[@id='filtr-shop']//input[@id='min_price']")
    max_price_ = find_element_by(driver, By.XPATH, "//div[@id='filtr-shop']//input[@id='max_price']")
    if min_price_.get_attribute('value') == min_price and max_price_.get_attribute(
                'value') == max_price and get_page_heading(driver).lower() == page_header.lower():
        assert True
    else:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


def verify_in_stock(driver, in_stock, page_header):
    wait_for(1)
    elem = find_element_by(driver, By.XPATH, "//span[@class='woof_remove_ppi']")
    if elem.text.lower() == in_stock.lower() and get_page_heading(driver).lower() == page_header.lower():
        assert True
    else:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


# Make a web page wait for n seconds
def wait_for(n):
    time.sleep(n)


def get_page_heading(driver):
    try:
        elem = get_wait(driver).until(EC.presence_of_element_located((By.XPATH, "//div[@class='container']//h1")))
        return elem.text
    except:
        save_screenshot(driver)
        assert False, 'check out screenshot ' + screenshot_name


# Find all dress names on the current page
def find_all_dresses(driver, class_n):
    dresses = find_all_elements_by(driver, By.CLASS_NAME, class_n)
    return dresses


def close_banner(driver):
    try:
        iframe_id = find_element_by(driver, By.XPATH, "//iframe[@title='Test Campaign']").get_attribute('id')

        driver.switch_to.frame(iframe_id)

        btn = find_element_by(driver, By.XPATH, "//button[@class='popup-close-btn']")
        wait_for(5)
        btn.click()

        driver.switch_to.default_content()
    except:
        pass


# email = "nycb-qa-test@riseserv.com"
def login(driver, email):
    pas = os.environ["ACC_PASS"]
    email_ = email
    click_element_by(driver, By.XPATH, "//div[@class='logo_sidebar']//a[@title='My Account']")
    find_element_by(driver, By.XPATH, "//input[@id='username']").send_keys(email_)
    find_element_by(driver, By.XPATH, "//input[@id='password']").send_keys(pas)
    try:
        click_element_by(driver, By.XPATH, "//button[@name='login']")
    except:
        pass


def verify_login(driver, email):
    username = email.split("@")[0]
    welcome_message = find_element_by(driver, By.XPATH, "//div[@class='woocommerce-MyAccount-content']/p[1]").text
    # try:
    if welcome_message == "Hello {} (not {}? Log out)".format(username, username):
        assert True
    else:
        save_screenshot(driver)
        assert False


def fill_alter_form(driver, country, state, city, zip, name, email):
    # country_ = get_wait(driver).until(EC.visibility_of_element_located((By.XPATH, "//input[@name='Country']")))
    # country_.send_keys(country + " " + datetime.now().strftime("%d_%m_%y-%H_%M_%S"))
    wait_for(2)
    country_ = find_element_by(driver, By.XPATH, "//input[@name='Country']")
    country_.send_keys(country + " " + datetime.now().strftime("%d_%m_%y-%H_%M_%S"))
    find_element_by(driver, By.XPATH, "//input[@name='State']").send_keys(state)
    find_element_by(driver, By.XPATH, "//input[@name='City']").send_keys(city)
    find_element_by(driver, By.XPATH, "//input[@name='ZIP']").send_keys(zip)
    find_element_by(driver, By.XPATH, "//input[@name='Name']").send_keys(name)
    find_element_by(driver, By.XPATH, "//input[@name='Email']").send_keys(email)

    try:
        click_element_by(driver, By.XPATH, "//input[@type='submit']")
    except:
        pass


def verify_alter_form(driver, message):
    # result = find_element_by(driver, By.XPATH, "//div[@id='wpcf7-f203261-p203260-o1']//div[@class='wpcf7-response-output']").text
    # result = get_wait(driver).until(EC.visibility_of_element_located((By.XPATH, "//div[@id='wpcf7-f203261-p203260-o1']//div[@class='wpcf7-response-output']"))).text
    # print(result)
    # if result == message:
    #     assert True
    # else:
    #     save_screenshot(driver)
    #     assert False, 'check out screenshot ' + screenshot_name
    pass


def logout(driver):
    click_element_by(driver, By.XPATH, "//div[@class='logo_sidebar fixed']//*[@title='My Account']")
    click_element_by(driver, By.XPATH, "//main//a[contains(text(), 'Logout')]")


def go_to_page(driver, page):
    driver.get(settings["url"] + '/' + '{}'.format(page))

